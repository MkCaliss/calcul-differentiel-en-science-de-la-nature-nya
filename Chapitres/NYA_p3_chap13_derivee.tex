%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Dérivée}
%
%
\begin{bloc}{m}
Nous savons déjà comment dériver les fonctions algébriques. Dans ce chapitre, nous verrons comment dériver les fonctions transcendantes. 

La \infobulle{définition de la fonction dérivée}{defDerivee} est toujours la même. Nous l'utiliserons pour trouver les dérivées des fonctions $f(x)=\ln(x)$, $f(x) = \sin(x)$ et $f(x) = \cos(x)$. Les autres dérivées seront déduites de ces dernières.
\end{bloc}%m
%
%
\section{Dérivées des fonctions logarithmiques et exponentielles}
%
%
\begin{bloc}{m}
Nous allons d'abord voir comment dériver les fonctions logarithmiques.
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée de la fonction $\ln(x)$]{thmDeriveeFctLn}[\dans{m}{10cm/20cm}\dans{bin}{24cm}]
Soit $f(x) = \ln(x)$. Alors $f'(x) = \dfrac{1}{x}$. \index{Théorème!dérivée de $\ln(x)$}

\preuve{

Puisque $\dom(\ln(x)) = ]0, \infty[$, il s'en suit que $x>0$.
\begingroup
\allowdisplaybreaks 
\begin{alignat*}{3}
f'(x) \ &= \ \dlim{h\to 0}{\dfrac{f(x+h)-f(x)}{h}} \hspace{2cm} && \text{\infobulle{définition de la dérivée}{defDerivee}} \\[0.3cm]
&= \ \dlim{h\to 0}{\dfrac{\ln(x+h)-\ln(x)}{h}} && \text{définition de $f$} \\[0.3cm]
 &= \ \dlim{h \to 0}{\dfrac{1}{h}\ln\paren*{\dfrac{x+h}{x}}} \hspace{2.5cm} && \text{\infobulle{propriété 2 des logarithmes}{propProprietesLog}}\\[0.3cm]
f'(x) &= \ \dlim{h \to 0}{\ln\paren*{\dfrac{x+h}{x}}^{\frac{1}{h}}} && \text{\infobulle{propriété 3 des logarithmes}{propProprietesLog}}\\[0.3cm]
&= \ \dlim{h \to 0}{\ln\paren*{\dfrac{x+h}{x}}^{\frac{1}{x}\frac{x}{h}}} \\[0.3cm]
&= \ \dlim{h \to 0}{\ln\paren*{\paren*{1+\dfrac{h}{x}}^{\frac{x}{h}}}^{\frac{1}{x}}} \hspace{2cm} \\[0.3cm]
&= \ \dlim{h \to 0}{\dfrac{1}{x}\ln\paren*{1+\dfrac{h}{x}}^{\frac{x}{h}}} && \text{\infobulle{propriété 3 des logarithmes}{propProprietesLog}}\\[0.3cm]
&= \ \dfrac{1}{x}\dlim{h \to 0}{\ln\paren*{1+\dfrac{h}{x}}^{\frac{x}{h}}} &&  \text{\infobulle{propriété 2 des limites}{thmLimiteProp}}\\[0.3cm]
&= \ \dfrac{1}{x}\ln\paren*{\dlim{h \to 0}{\paren*{1+\dfrac{h}{x}}^{\frac{x}{h}}}} && \text{\infobulle{propriété 4 des limites}{thmLimiteProp}}\\[0.3cm]
\intertext{Nous allons poser $z=\dfrac{x}{h}$. Lorsque $h$ tend vers $0$, $z$ tend vers plus ou moins l'infini.} \\
&= \ \dfrac{1}{x}\ln\paren*{ \ \dlim{z \to \pm \infty}{\paren*{1+\dfrac{1}{z}}^{z}} \ } \\[0.3cm]
&= \ \dfrac{1}{x} \ln(e) && \text{\infobulle{définition de $e$}{eqDefNbE} (\ref{eqDefNbE})} \\
&= \ \dfrac{1}{x}
\end{alignat*}
\endgroup
\finDemo
}
\end{thm}
\dans{nm}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée des fonctions logarithmiques]{thmDeriveeFctLog}
Soit $f(x) = \log_a(x)$. Alors $f'(x) = \dfrac{1}{x\ln(a)}$. \index{Théorème!dérivée de $\log_a(x)$} 

\preuve{

Dans les logarithmes, pour passer d'une base à une autre, nous utilisons la formule suivante.%
\[\log_a(x) = \dfrac{\ln(x)}{\ln(a)}\]%
Ainsi,% 
\[\paren*{\log_a(x)}' = \paren*{\dfrac{\ln(x)}{\ln(a)}}' = \dfrac{1}{\ln(a)}\paren*{\ln(x)}' = \dfrac{1}{x\ln(a)}\]

\finDemo
}
\end{thm}
\dans{n}{\vfill}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
À l'aide de la dérivée des fonctions logarithmiques, il nous est maintenant possible de trouver la dérivée des fonctions exponentielles.
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée des fonctions exponentielles]{thmDeriveeFctExp}
Soit $f(x) = a^x$. Alors $f'(x) = a^x \ln(a)$. \index{Théorème!dérivée de $a^x$}

\preuve{

Nous souhaitons trouver $f'(x)$ sachant que $f(x)=a^x$. En prenant le logarithme de chaque côté nous obtenons%
\[\log_a(f(x)) = x\text{ .}\]%
Il s'agit d'une équation implicite. Nous sommes en mesure de dériver de chaque côté.
\begin{alignat*}{4}
 && \log_a(f(x)) &= x \\
\Rightarrow \hspace*{1cm} &&\dfrac{1}{f(x)\ln(a)}\dfrac{\dd }{\dd x}f(x) &= \dfrac{\dd }{\dd x}x  \\
\Rightarrow \hspace*{1cm} &&\dfrac{f'(x)}{f(x)\ln(a)} &= 1
\end{alignat*}
Il reste à isoler $f'(x)$.%
\[f'(x) = f(x)\ln(a) = a^x\ln(a)\]%
\finDemo
}
\end{thm}
\dans{n}{\vfill}
\dans{m}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
En particulier, la dérivée de la fonction exponentielle en base $e$ est la fonction elle-même, car $\ln(e)=1$.%
\[\paren*{e^x}' = e^x\]%

\begin{exemple}
\QS{Trouver la dérivée de la fonction $f(x) = (x^2+1)\cdot 2^x$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{3}
f'(x) &= (x^2+1)' \cdot 2^x + (x^2+1)\paren*{2^x}'\hspace{2cm} && \text{\infobulle{théorème  \ref{thmDeriveeMultiplication}}{thmDeriveeMultiplication}} \\[0.3cm]
&= x2^{x+1}+(x^2+1) 2^x \ln(2)&& \text{\infobulle{théorème \ref{thmDeriveeFctExp}}{thmDeriveeFctExp}}
\end{alignat*}
}
\end{exemple}

\begin{exemple}
\QS{Trouver la dérivée de la fonction $f(x) = \log_3(\sqrt{x}+1)$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{3}
f'(x) &= \dfrac{\paren*{\sqrt{x}+1}'}{\ln(3)(\sqrt{x}+1)}\hspace{2cm} && \text{théorèmes \infobulle{\ref{thmDeriveeChaine}}{thmDeriveeChaine} et \infobulle{\ref{thmDeriveeFctLog}}{thmDeriveeFctLog}} \\[0.3cm]
&= \dfrac{1}{2\ln(3)\sqrt{x}\paren*{\sqrt{x}+1}} && \text{\infobulle{théorème \ref{thmDeriveePuissanceNreel}}{thmDeriveePuissanceNreel}}
\end{alignat*}
}
\end{exemple}
\end{bloc}%m
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?time_continue=1&v=YUEO8Ufn9Hw}{Démonstration de la dérivée de l'exponentielle et exemples.}
\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Déterminer la dérivée des fonctions suivantes.}
\debutSousQ
\QR{$f(x)=x^e$}{$f'(x)=ex^{e-1}$}  
\QR{$f(x)=e^x$}{$f'(x)=e^x$} 
\QR{$f(x)=e^7$}{$f'(x)=0$} 
\QR{$f(x)=\ln(e)$}{$f'(x)=0$}  
\QR{$f(x)=\ln(e^x)$}{$f'(x)=1$}  
\QR{$f(x)=\log_5(x^2)$}{$f'(x)=\dfrac{2}{x\ln(5)}$}
\QR{$f(x)=\ln\paren*{\dfrac{x}{1+x^2}}$}{$f'(x)=\dfrac{1-x^2}{x(1+x^2)}$} 
\QR{$f(x)=\sqrt{\log(x)}$}{$f'(x)=\dfrac{1}{2x\ln(10)\sqrt{\log(x)}}$}  
\QR{$f(x)=(\ln(x))^3$}{$f'(x)=\dfrac{3(\ln(x))^2}{x}$} 
\QR{$f(x)=x^3\ln(x)$}{$f'(x)=3x^2\ln(x)+x^2$}  
\QR{$f(x)=e^{7x}$}{$f'(x)=7e^{7x}$}  
\QR{$f(x)=3^{x^2+1}$}{$f'(x)=2x\cdot 3^{x^2+1}\cdot \ln(3)$}  
\QR{$f(x)=x^2\cdot 2^x$}{$f'(x)=2^{x+1}x+2^xx^2\ln(2)$}  
\QR{$f(x)=2^{\sqrt{x}}$}{$f'(x)=\dfrac{2^{\sqrt{x}}\ln(2)}{2\sqrt{x}}$}  
\QR{$f(x)=\dfrac{e^x}{\ln(x)}$}{$f'(x)=\dfrac{xe^x\ln(x)-e^x}{x(\ln(x))^2}$}  
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Dérivées des fonctions trigonométriques}
%
%
\begin{bloc}{m}
Nous allons maintenant voir les dérivées des fonctions trigonométriques.
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée de la fonction sinus]{thmDeriveeFctSinus}
Soit $f(x) = \sin(x)$. Alors $f'(x) = \cos(x)$. \index{Théorème!dérivée de $\sin(x)$}

\preuve{ 
\begin{alignat*}{3}
f'(x) \ &= \ \dlim{h\to 0}{\dfrac{f(x+h)-f(x)}{h}} \hspace{6cm} && \text{\infobulle{définition de la dérivée}{defDerivee}} \\[0.3cm]
&= \ \dlim{h\to 0}{\dfrac{\sin(x+h)-\sin(x)}{h}} && \text{définition de $f$} \\[0.3cm]
&= \ \dlim{h\to 0}{\dfrac{\sin(x)\cos(h) + \cos(x)\sin(h)-\sin(x)}{h}} && \text{\infobulle{identité trigonométrique}{propAutresIdentitesTrigo}} \\[0.3cm]
&= \ \dlim{h\to 0}{\dfrac{\sin(x)\paren*{\cos(h)-1}+\cos(x)\sin(h)}{h}} && \text{mise en évidence} \\[0.3cm]
&= \ \dlim{h\to 0}{\dfrac{\sin(x)\paren*{\cos(h)-1}}{h}}+\dlim{h\to 0}{\dfrac{\cos(x)\sin(h)}{h}}&& \text{\infobulle{propriété 1 des limites}{thmLimiteProp}} \\[0.3cm]
&= \ \sin(x)\dlim{h\to 0}{\dfrac{\paren*{\cos(h)-1}}{h}}+\cos(x)\dlim{h\to 0}{\dfrac{\sin(h)}{h}} && \text{\infobulle{propriété 2 des limites}{thmLimiteProp}} \\[0.3cm]
&= \ \sin(x)\cdot 0 +\cos(x) \cdot 1 && \text{théorèmes \infobulle{\ref{thmLimiteSinxSurx}}{thmLimiteSinxSurx} et \infobulle{\ref{thmLimiteUnMoinsCosxSurx}}{thmLimiteUnMoinsCosxSurx}}\\[0.3cm]
&=\ \cos(x)
\end{alignat*}
\finDemo
}
\end{thm}
\dans{n}{\vfill}
\end{bloc}%bimn
%
%
\begin{bloc}{bm}[Remarque]
Dans la preuve précédente, nous utilisons un résultat du chapitre précédent concernant $\lim_{x\to 0}\frac{\sin(x)}{x}$. Il est très important de remarquer que dans le calcul de cette limite, $x$ représente la mesure d'un angle en \textbf{radians}. Ainsi, la dérivée de la fonction sinus obtenue au théorème précédent est valide seulement si l'angle est en radians. Cette remarque s'applique à toutes les dérivées des fonctions trigonométriques.
\dans{m}{\newpage}
\end{bloc}%bm
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée de la fonction cosinus]{thmDeriveeFctCosinus}
Soit $f(x) = \cos(x)$. Alors $f'(x) = -\sin(x)$. \index{Théorème!dérivée de $\cos(x)$}

\preuve{
\begin{alignat*}{3}
f'(x) \ &= \ \dlim{h\to 0}{\dfrac{f(x+h)-f(x)}{h}} \hspace{6cm} && \text{\infobulle{définition de la dérivée}{defDerivee}} \\[0.3cm]
&= \ \dlim{h\to 0}{\dfrac{\cos(x+h)-\cos(x)}{h}} && \text{définition de $f$} \\[0.3cm]
&= \ \dlim{h\to 0}{\dfrac{\cos(x)\cos(h)-\sin(x)\sin(h)-\cos(x)}{h}} && \text{\infobulle{identité trigonométrique}{propAutresIdentitesTrigo}} \\[0.3cm]
&= \ \dlim{h\to 0}{\dfrac{\cos(x)(\cos(h)-1)-\sin(x)\sin(h)}{h}} && \text{mise en évidence} \\[0.3cm]
&= \ \dlim{h\to 0}{\dfrac{\cos(x)(\cos(h)-1)}{h}}-\dlim{h\to 0}{\frac{\sin(x)\sin(h)}{h}} && \text{\infobulle{propriété 1 des limites}{thmLimiteProp}} \\[0.3cm]
&= \ \cos(x)\dlim{h\to 0}{\dfrac{(\cos(h)-1)}{h}}-\sin(x)\dlim{h\to 0}{\frac{\sin(h)}{h}} && \text{\infobulle{propriété 2 des limites}{thmLimiteProp}} \\[0.3cm]
&= \ \cos(x)\cdot 0 -\sin(x) \cdot 1 && \text{théorèmes \infobulle{\ref{thmLimiteSinxSurx}}{thmLimiteSinxSurx} et \infobulle{\ref{thmLimiteUnMoinsCosxSurx}}{thmLimiteUnMoinsCosxSurx}}\\[0.3cm]
&=\ -\sin(x)
\end{alignat*}
\finDemo
}
\end{thm}
\dans{m}{\newpage}\dans{n}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée de la fonction tangente]{deriveeFctTangente}
Soit $f(x) = \tan(x)$. Alors $f'(x) = \sec^2(x)$. \index{Théorème!dérivée de $\tan(x)$}

\preuve{

Puisque $\tan(x) = \dfrac{\sin(x)}{\cos(x)}$, nous pouvons trouver sa dérivée en utilisant la formule de la \infobulle{dérivée d'un quotient}{thmDeriveeQuotient} ainsi que les formules de dérivation des fonctions \infobulle{sinus}{thmDeriveeFctSinus} et \infobulle{cosinus}{thmDeriveeFctCosinus}.
\begin{alignat*}{3}
\paren*{\dfrac{\sin(x)}{\cos(x)}}' &= \dfrac{(\sin(x))'\cos(x)-\sin(x)(\cos(x))'}{\cos^2(x)}\\[0.3cm]
&= \dfrac{\cos^2(x)+\sin^2(x)}{\cos^2(x)} \\[0.3cm]
&= \dfrac{1}{\cos^2(x)} \hspace*{3cm} && \text{\infobulle{identité trigonométrique}{propIdentitesTrigoBase}}\\[0.3cm]
&= \sec^2(x) 
\end{alignat*} 
\finDemo
}
\end{thm}
\dans{n}{\vfill}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée des autres fonctions trigonométriques]{thmDeriveeAutresFctTrigo}
\index{Théorème!dérivée de $\sec(x)$} \index{Théorème!dérivée de $\cosec(x)$} \index{Théorème!dérivée de $\cotan(x)$}
\begin{liste}[itSep=0.3cm]
\item $\dfrac{\dd }{\dd x}\cotan(x) = -\cosec^2(x)$ 
\item $\dfrac{\dd }{\dd x}\sec(x) = \sec(x)\tan(x)$ 
\item $\dfrac{\dd }{\dd x}\cosec(x) = -\cosec(x)\cotan(x)$ 
\end{liste}

\preuve{

On utilise la même technique que pour la démonstration de la dérivée de la fonction tangente.
}
\end{thm}
\dans{m}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Trouver la dérivée de la fonction $f(x) = x\sin(x)$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{3}
f'(x) &= (x)'\sin(x) + x \paren*{\sin(x)}' \hspace{2cm} && \text{\infobulle{dérivée d'un produit}{thmDeriveeMultiplication}} \\[0.2cm]
&= \sin(x)+x\cos(x) && \text{\infobulle{dérivée de la fonction sinus}{thmDeriveeFctSinus}}
\end{alignat*}
}
\end{exemple}

\begin{exemple}
\QS{Trouver la dérivée de la fonction $f(x) = \dfrac{\sqrt{x}+1}{\cotan(x)}$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{3}
f'(x) &= \dfrac{\paren*{\sqrt{x}+1}'\cotan(x)-\paren*{\sqrt{x}+1}\paren*{\cotan(x)}'}{\cotan^2(x)} \hspace{2cm} && \text{\infobulle{dérivée d'un quotient}{thmDeriveeQuotient}} \\[0.3cm]
&= \dfrac{\frac{\cotan(x) }{2\sqrt{x}}+ \paren*{\sqrt{x}+1}\cosec^2(x)}{\cotan^2(x)} && \text{théorèmes \infobulle{\ref{thmDeriveePuissanceNreel}}{thmDeriveePuissanceNreel} et \infobulle{\ref{thmDeriveeAutresFctTrigo}}{thmDeriveeAutresFctTrigo}} \\[0.3cm]
&= \dfrac{\cotan(x) + 2\paren*{x+\sqrt{x}}\cosec^2(x)}{2\sqrt{x}\cotan^2(x)}
\end{alignat*}
}
\end{exemple}

\begin{exemple}
\QS{Trouver la dérivée de la fonction $f(x) = \log_2\paren*{\sec(x)} - \cos(x^2+e^x)$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{3}
f'(x) &= \paren*{\log_2\paren*{\sec(x)}}' - \paren*{\cos(x^2+e^x)}'\hspace{2cm} && \text{\infobulle{théorème \ref{thmDeriveeAddition}}{thmDeriveeAddition}} \\[0.3cm]
&= \dfrac{\paren*{\sec(x)}'}{\ln(2)\sec(x)} + \sin(x^2+e^x)\paren*{x^2+e^x}'&& \text{théorèmes \infobulle{\ref{thmDeriveeChaine}}{thmDeriveeChaine}, \infobulle{\ref{thmDeriveeFctLog}}{thmDeriveeFctLog} et \infobulle{\ref{thmDeriveeFctCosinus}}{thmDeriveeFctCosinus}} \\[0.3cm]
&= \dfrac{\sec(x)\tan(x)}{\ln(2)\sec(x)} + \paren*{2x+e^x}\sin(x^2+e^x) \\[0.3cm]
&= \dfrac{\tan(x)}{\ln(2)} + \paren*{2x+e^x}\sin(x^2+e^x) && \text{théorèmes \infobulle{  \ref{thmDeriveeAutresFctTrigo}}{thmDeriveeAutresFctTrigo} et \infobulle{\ref{thmDeriveeFctExp}}{thmDeriveeFctExp}}
\end{alignat*}
}
\end{exemple}
\end{bloc}%m
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?v=5WZ__ZMlPOM}{Démonstration de la formule de dérivation pour cosinus et exemple.}

\href{https://www.youtube.com/watch?time_continue=1&v=2D8U6pfQ0Z8}{Exemples de dérivées de fonctions trigonométriques.}
\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Déterminer la dérivée des fonctions suivantes.}
\debutSousQ
\QR{$f(x)=x \cos(x)$}{$f'(x)=\cos(x) - x \sin(x)$}
\QR{$f(x)=\cos^2(2-5x)$}{$f'(x)=10\cos(2-5x)\sin(2-5x)$} 
\QR{$f(x)=\cosec(2x)$}{$f'(x)=-2\cosec(2x)\cotan(2x)$}  
\QR{$f(x)=\sin\paren*{\sqrt{1+\cos(x)}}$}{$f'(x)=-\dfrac{\sin(x) \cos(\sqrt{1+\cos(x)})}{2\sqrt{1+\cos(x)}}$} 
\QR{$f(x)=\tan(x^2+1)$}{$f'(x)=2x \sec^2(x^2+1)$} 
\QR{$f(x)=x^3\sin^2(5x)$}{$f'(x)=3x^2\sin^2(5x)+10x^3\sin(5x)\cos(5x)$}  
\QR{$f(x)=\sec\paren*{\dfrac{1}{x^2}}$}{$f'(x)=\dfrac{{-2}\sec\paren*{\frac{1}{x^2}}\tan\paren*{\frac{1}{x^2}}}{x^3}$} 
\QR{$f(x)=\cos^3(\sin(4x))$}{$f'(x)=-12\cos^2(\sin(4x))\sin(\sin(4x))\cos(4x)$}
\QR{$f(x)=\dfrac{5-\cos(x)}{5+\sin(x)}$}{$f'(x)=\dfrac{1+5\sin(x)-5\cos(x)}{(5+\sin(x))^2}$}  
\QR{$f(x)=2^{\tan(x)}$}{$f'(x)=\ln(2)2^{\tan(x)}\sec^2(x)$}
\QR{$f(x)=\ln(\sin(x^2))$}{$f'(x)=2x \cotan(x^2)$}
\finSousQ

\Q{Donner l'équation de la tangente à la courbe $f(x)$ en $x=a$.}
\debutSousQ
\QR{$f(x)=\sin(x)$ et $a=0$}{$y=x$}
\QR{$f(x)=\sin(x)$ et $a=\pi$}{$y=-x+\pi$}
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Dérivées des fonctions trigonométriques réciproques}
%
%
\begin{bloc}{m}
Dans cette section, nous allons présenter la dérivée des fonctions trigonométriques réciproques.
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée de la fonction arc sinus]{thmDeriveeArcsinus}[\dans{m}{19cm/24cm}\dans{bin}{24cm}]
Soit $f(x) = \arcsin(x)$. Alors $f'(x) = \dfrac{1}{\sqrt{1-x^2}}$. \index{Théorème!dérivée de $\arcsin(x)$}

\preuve{

Nous souhaitons trouver $f'(x)$ sachant que $f(x)=\arcsin(x)$. En prenant le sinus de chaque côté nous obtenons
\begin{align}
\sin\paren*{f(x)} = \sin\paren*{\arcsin(x)} = x\text{ .} \label{eqArcsin}
\end{align}
Il s'agit d'une équation implicite. Nous sommes en mesure de dériver de chaque côté.%
\[\cos\paren*{f(x)}f'(x) = 1\text{ .}\]%
Il reste à isoler $f'(x)$.%
\[f'(x) = \dfrac{1}{\cos\paren*{f(x)}}\]%
Nous souhaitons exprimer $f(x)$ en terme de $x$. Dans l'\autoref{eqArcsin}, nous pouvons interpréter $f(x)$ comme un angle et, par la définition du sinus dans le triangle rectangle, nous trouvons% 
\[\sin\paren*{f(x)} = x = \dfrac{x}{1} = \dfrac{\text{côté opposé}}{\text{hypothénuse}} \text{ .}\]
En utilisant le théorème de Pythagore, nous trouvons la mesure du côté adjacent. Nous obtenons la figure suivante.
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_13_demo_deriveeArcsin_man.pdf}
\end{center}
Dans le triangle, nous trouvons% 
\[\cos\paren*{f(x)} = \dfrac{\text{côté adjacent}}{\text{hypothénuse}} = \dfrac{\sqrt{1-x^2}}{1} = \sqrt{1-x^2}\text{ .}\] 
Ainsi%
\[f'(x) = \dfrac{1}{\cos\paren*{f(x)}} = \dfrac{1}{\sqrt{1-x^2}}\text{ .}\]
\finDemo
}
\end{thm}
\dans{n}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée des fonctions\dans{i}{\newline} trigonométriques réciproques]{thmDeriveeAutresFctTrigoReciproque}
\index{Théorème!dérivée de $\arccos(x)$} \index{Théorème!dérivée de $\arctan(x)$} \index{Théorème!dérivée de $\arcsec(x)$} \index{Théorème!dérivée de $\arccosec(x)$} \index{Théorème!dérivée de $\arccotan(x)$}
\begin{liste}[itSep=\dans{mni}{0.3cm}\dans{b}{0.2cm}]
\item $\dfrac{\dd}{\dd x}\arccos(x) =-\dfrac{1}{\sqrt{1-x^2}}$ 
\item $\dfrac{\dd}{\dd x}\arctan(x) = \dfrac{1}{1+x^2}$  
\item $\dfrac{\dd}{\dd x}\arccotan(x) = -\dfrac{1}{1+x^2}$   
\item $\dfrac{\dd}{\dd x}\arcsec(x) = \dfrac{1}{|x|\sqrt{x^2-1}}$  
\item $\dfrac{\dd}{\dd x}\arccosec(x) = -\dfrac{1}{|x|\sqrt{x^2-1}}$  
\end{liste}

\preuve{

On utilise la même technique que dans la démonstration de la dérivée de la fonction arc sinus.
}
\end{thm}
\dans{n}{\vfill}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Trouver la dérivée de la fonction $f(x) = \arccos(x)\arctan(x)$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{3}
f'(x) &= \paren*{\arccos(x)}'\arctan(x) + \arccos(x)\paren*{\arctan(x)}'\hspace{2cm} && \text{\infobulle{théorème \ref{thmDeriveeMultiplication}}{thmDeriveeMultiplication}} \\[0.3cm]
&= -\dfrac{\arctan(x)}{\sqrt{1-x^2}} + \dfrac{\arccos(x)}{1+x^2} && \text{\infobulle{théorème \ref{thmDeriveeAutresFctTrigoReciproque}}{thmDeriveeAutresFctTrigoReciproque}}
\end{alignat*}
}
\end{exemple}

\begin{exemple}
\QS{Trouver la dérivée de la fonction $f(x) = \arcsin(\ln(x))$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{3}
f'(x) &= \dfrac{1}{\sqrt{1-\ln^2(x)}}\paren*{\ln(x)}'\hspace{2cm} && \text{théorèmes \infobulle{\ref{thmDeriveeChaine}}{thmDeriveeChaine} et \infobulle{\ref{thmDeriveeArcsinus}}{thmDeriveeArcsinus}} \\[0.3cm]
&= \dfrac{1}{x\sqrt{1-\ln^2(x)}} && \text{\infobulle{théorème \ref{thmDeriveeFctLog}}{thmDeriveeFctLog}}
\end{alignat*}
}
\end{exemple}
\end{bloc}%m
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?time_continue=1&v=P2xBdBsX5LY}{Démonstration des formules de dérivation pour arc sinus et arc sécante et exemples.}
\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}

\Q{Déterminer la dérivée des fonctions suivantes.}
\debutSousQ
\QR{$f(x)=\arctan(x^3)$}{$f'(x)=\dfrac{3x^2}{1+x^6}$} 
\QR{$f(x)=\arccosec(2x)$}{$f'(x)=\dfrac{-1}{|x|\sqrt{4x^2-1}}$}
\QR{$f(x)=\arcsin(x)+\arccos(x)$}{$f'(x)=0$}   
\QR{$f(x)=x \arcsin\paren*{\dfrac{1}{x}}$}{$f'(x)=\arcsin\paren*{\dfrac{1}{x}} - \dfrac{x}{|x|\sqrt{x^2-1}}$}
\QR{$f(x)=(\tan(x))^{-1}$}{$f'(x)=-\cosec^2(x)$} 
\QR{$f(x)=\arccotan(\sqrt{x})$}{$f'(x)=\dfrac{-1}{2(1+x)\sqrt{x}}$}  
\QR{$f(x)=(\arcsec(x^2+1))^2$}{$f'(x)=\dfrac{4x \arcsec(x^2+1)}{(x^2+1)\sqrt{(x^2+1)^2-1}}$}
\QR{$f(x)=e^{\arctan(x)}$}{$f'(x)=\dfrac{e^{\arctan(x)}}{1+x^2}$}  
\QR{$f(x)=\ln(\arcsin(x))$}{$f'(x)=\dfrac{1}{\arcsin(x) \sqrt{1-x^2}}$}  
\finSousQ

\Q{Déterminer $\dfrac{\textrm{d}y}{\textrm{d}x}$.}
\debutSousQ
\QR{$x^3+x \arctan(y)=e^y$}{$\dfrac{\textrm{d}y}{\textrm{d}x}=\dfrac{(3x^2+\arctan(y))(1+y^2)}{e^y+e^yy^2-x}$}
\QR{$y=\sin(e^{\cos(x)}-\ln(x^2))$}{$\dfrac{\textrm{d}y}{\textrm{d}x}=-\cos(e^{\cos(x)}-\ln(x^2))\dfrac{xe^{\cos(x)}\sin(x)+2}{x}$}
\QR{$\arcsin(y)=2^x$}{$\dfrac{\textrm{d}y}{\textrm{d}x}=2^x\ln(2)\sqrt{1-y^2}=2^x\ln(2)\big|\cos(2^x)\big|$}
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Dérivation logarithmique}
%
%
\begin{bloc}{bm}[Dérivation logarithmique]
Il y a encore un type de fonctions simples que nous ne sommes pas en mesure de dériver avec les théorèmes que nous avons vus. Il s'agit des fonctions de la forme suivante.%
\[y = \paren[\Big]{g(x)}^{h(x)}\]
\end{bloc}%bm
%
%
\begin{bloc}{m}
Il ne s'agit pas d'une forme polynomiale, car l'exposant n'est pas un nombre naturel et il ne s'agit pas d'une forme exponentielle, car la base n'est pas un nombre réel positif. 

Pour dériver ce genre de fonction, nous commencerons par appliquer un logarithme de chaque côté de l'égalité. Par les propriétés des logarithmes, nous pourrons ainsi descendre l'exposant devant le logarithme. Il ne restera ensuite qu'à dériver implicitement de chaque côté. Cette méthode se nomme la \emph{dérivation logarithmique}.\index{Dérivation logarithmique}

\begin{exemple}
\QS{Trouver la dérivée de $f(x) = x^{\sin(x)}$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{4}
&& f(x) &= x^{\sin(x)} \\[0.3cm]
\Rightarrow \hspace*{0.5cm} && \ln\paren[\big]{f(x)} &= \ln\paren*{x^{\sin(x)}} \hspace{4.5cm} && \text{appliquer un logarithme de chaque côté}\\[0.3cm]
\Rightarrow \hspace*{0.5cm} && \ln\paren[\big]{f(x)} &= \sin(x)\ln(x) && \text{\infobulle{propriété des logarithmes}{propProprietesLog}}\\[0.3cm]
\Rightarrow \hspace*{0.5cm} && \dfrac{f'(x)}{f(x)} &= \cos(x)\ln(x) + \dfrac{\sin(x)}{x} && \text{dériver de chaque côté} \\[0.3cm]
\Rightarrow \hspace*{0.5cm} && f'(x) &= \paren*{\cos(x)\ln(x) + \dfrac{\sin(x)}{x}}x^{\sin(x)} && \text{isoler $f'(x)$ et définition de $f$}
\end{alignat*}
}
\end{exemple}

\begin{exemple}
\QS{Trouver la dérivée de $f(x) = \paren*{\tan(x)}^{e^x}$.}{
\setlength{\belowdisplayskip}{0pt}%
\begingroup
\allowdisplaybreaks
\begin{alignat*}{4}
 && f(x) &= \paren[\big]{\tan(x)}^{e^x} \\
\intertext{Nous appliquons un logarithme de chaque côté.}
\Rightarrow \hspace*{0.5cm} &&  \ln\paren[\big]{f(x)} &= \ln\paren[\Big]{\paren[\big]{\tan(x)}^{e^x}}\\[0.3cm]
\Rightarrow \hspace*{0.5cm} &&  \ln\paren[\big]{f(x)} &= e^x\ln\paren[\big]{\tan(x)} && \text{\infobulle{propriété des logarithmes}{propProprietesLog}}\\[0.3cm]
\Rightarrow \hspace*{0.5cm} &&  \dfrac{f'(x)}{f(x)} &= e^x\ln\paren[\big]{\tan(x)} + \dfrac{e^x \sec^2(x)}{\tan(x)} && \text{dériver de chaque côté} \\[0.3cm]
\intertext{Nous allons isoler $f'(x)$ et utiliser la définition de $f$.}
\Rightarrow \hspace*{0.5cm} &&  f'(x) &= \paren*{e^x\ln\paren[\big]{\tan(x)} + \dfrac{e^x}{\sin(x)\cos(x)}}\paren[\big]{\tan(x)}^{e^x} 
\end{alignat*}
\endgroup
}
\end{exemple}

Dans l'exemple précédent, nous avons fait une simplification avec la sécante carrée et la tangente. Ce genre de simplification doit toujours être effectué. 
\end{bloc}%m


\begin{bloc}{bn}
\begin{exemple}

\QR{Trouver la dérivée de la fonction suivante.% 
\[f(x) = x^{\tan(x)}\]}{$f'(x) = \paren*{\sec^2(x)\ln(x)+\dfrac{\tan(x)}{x}}x^{\tan(x)}$}[\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{m}
À l'aide de la dérivation logarithmique, il est maintenant possible de démontrer le \infobulle{théorème \ref{thmDeriveePuissanceNreel}}{thmDeriveePuissanceNreel}, que nous utilisons depuis un certain temps déjà. 
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{thmSansNum}[\hspace*{-0.5cm}\ref*{thmDeriveePuissanceNreel}~~~Dérivée d'une puissance avec \dans{bi}{\newline\hspace*{0.52cm}}exposant réel]{thmDeriveePuissanceNreelPreuve}
Si $f(x) = x^k$, où $k \in \mathds{R}$, alors $f'(x) = kx^{k-1}$. \index{Théorème!dérivée d'une puissance réelle de $x$} 

\preuve{
\begin{alignat*}{3}
&& f(x) &= x^k \\[0.3cm]
\Rightarrow \hspace*{1cm} && \ln\paren[\big]{f(x)} &= \ln\paren[\big]{x^k} \\[0.3cm]
\Rightarrow \hspace*{1cm} && \ln\paren[\big]{f(x)} &= k\ln(x) \\[0.3cm]
\Rightarrow \hspace*{1cm} && \dfrac{f'(x)}{f(x)} &= \dfrac{k}{x} \\[0.3cm]
\Rightarrow \hspace*{1cm} && f'(x) &= \dfrac{kf(x)}{x} \\[0.3cm]
\Rightarrow \hspace*{1cm} && f'(x) &= \dfrac{kx^k}{x} \\[0.3cm]
\Rightarrow \hspace*{1cm} && f'(x) &= kx^{k-1}
\end{alignat*}
\finDemo
}
\end{thmSansNum}
\dans{n}{\vfill}
\end{bloc}%bimn
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}[allowframebreaks]{bn}
\begin{exercice}

\Q{Trouver la dérivée des fonctions suivantes.}
\debutSousQ[itSep=0.3cm, nbCols=2]
\QR{$f(x) = 4^x\cos(x)$}{$f'(x) = 4^x\ln(4)\cos(x)-4^x\sin(x)$}

\QR{$f(x) = \sin^2(x)$}{$f'(x) = 2\sin(x)\cos(x)$}

\QR{$f(x) = \arctan(x^2)$}{$f'(x) = \dfrac{2x}{1+x^4}$}
\framebreak
\QR{$f(x) = \tan\paren[\big]{\ln(x)} - x\log(x)$}{$f'(x) = \dfrac{\sec^2\paren[\big]{\ln(x)}}{x} - \log(x) - \dfrac{1}{\ln(10)}$}

\QR{$f(x) = \cosec\paren[\big]{\arcsin(x)}$}{$f'(x) = -\dfrac{\cosec\paren[\big]{\arcsin(x)}\cotan\paren[\big]{\arcsin(x)}}{\sqrt{1-x^2}}$}

\QR{$f(x) = \ln\paren*{\ln(x)}$}{$f'(x) = \dfrac{1}{x\ln(x)}$}
\framebreak
\QR{$f(x) = \dfrac{\sin(x^2+x)}{e^x-x}$}{$f'(x) = \dfrac{\cos(x^2+x)(2x+1)(e^x-x) - \sin(x^2+x)(e^x-1)}{\paren[\big]{e^x-x}^2}$}

\QR[4cm]{$f(x) = x^x$}{$f'(x) = \paren[\big]{\ln(x)+1} x^x$}

\QR[4cm]{$f(x) = e^{x^2}$}{$f'(x) = 2xe^{x^2}$}

\QR{$f(x) =  \paren[\big]{\arcsin(x)}^{\cos(x)}$}{\scalebox{0.75}{$f'(x) = \paren[\Big]{-\sin(x)\ln\paren[\big]{\arcsin(x)}+\dfrac{\cos(x)}{\arcsin(x)\sqrt{1-x^2}}}\paren[\big]{\arcsin(x)}^{\cos(x)}$}}

\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\begin{bloc}{b}[Activité]
\begin{center}
Activité en équipes
\end{center}
\end{bloc}%b
%
%
\begin{bloc}{b}[Activité]
\vspace*{-0.1cm}
{\center 
\textcolor{blue}{Équipes bleues}
\par}
\vspace*{-0.3cm}
Inventer une fonction qui respecte les critères suivants :
\begin{liste}[type=itemize, itSep=-0.25cm]
\item La fonction doit être un quotient. 
\item Il doit y avoir une fonction trigonométrique affectée d'une puissance. 
\item L'argument de la fonction trigonométrique doit être un polynôme de degré minimal trois et d'au moins deux termes.
\end{liste}
\vspace*{-0.1cm}
{\center 
\textcolor{red}{Équipes rouges}
\par}
\vspace*{-0.3cm}
Inventer une fonction qui respecte les critères suivants :
\begin{liste}[type=itemize, itSep=-0.25cm]
\item La fonction doit être un produit.
\item Il doit y avoir une fonction exponentielle. 
\item L'exposant de la fonction exponentielle doit être une fonction trigonométrique.
\item Il doit y avoir une fonction polynomiale, d'au moins deux termes, affectée d'une puissance.
\end{liste}
\end{bloc}%b
%
%
\begin{bloc}{b}[Activité]
\begin{center} \textcolor{blue}{Équipes bleues} \end{center}
\vfill
Dériver la fonction d'une équipe rouge.
\vfill
\hrulefill
\begin{center} \textcolor{red}{Équipes rouges} \end{center}
\vfill
Dériver la fonction d'une équipe bleue.
\vfill
\end{bloc}%b
%
%
\begin{bloc}{b}[Activité]
\begin{center} \textcolor{blue}{Équipes bleues} \end{center}
\vfill
Corriger la dérivée de la fonction que vous avez inventée.
\vfill
\hrulefill
\begin{center} \textcolor{red}{Équipes rouges} \end{center}
\vfill
Corriger la dérivée de la fonction que vous avez inventée.
\vfill
\end{bloc}%b
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Déterminer la dérivée des fonctions suivantes.}
\debutSousQ
\QR{$f(x)=(x^2+1)^x$}{$f'(x)=(x^2+1)^x\paren*{\ln(x^2+1)+\dfrac{2x^2}{x^2+1}}$}  
\QR{$f(x)=(\ln(x))^{\sin(x)}$}{$f'(x)=(\ln(x))^{\sin(x)}\paren*{\cos(x)\ln(\ln(x))+\dfrac{\sin(x)}{x\ln(x)}}$}
\QR{$f(x)=\cos(x^x)$}{$f'(x)=-x^x(\ln(x)+1)\sin(x^x)$\dans{e}{\columnbreak}}
\finSousQ
\end{secExercices}
\end{bloc}%eim
