%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Dérivée}
%
%
\section{À l'aide de la définition}
%
%
\begin{bloc}{m}
Nous savons déjà comment dériver les fonctions polynomiales. Dans ce chapitre, nous verrons comment dériver les fonctions rationnelles et les fonctions contenant des racines. 

La \infobulle{définition de la fonction dérivée}{defDerivee} est toujours la même. On peut l'utiliser pour trouver la dérivée d'une fonction algébrique.

\begin{exemple}
\QS{Trouver la dérivée de la fonction $f(x)=\displaystyle \frac{1}{x}$ en utilisant la définition de la dérivée.
}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{4}
 f'(x) =& \ \dlim{h \to 0}{\frac{f(x+h) - f(x)}{h}}  &&\hspace{2cm}   \text{\infobulle{définition de la dérivée}{defDerivee}} \\[0.3cm]
=& \ \dlim{h \to 0}{\displaystyle \frac{\displaystyle \frac{1}{x+h}-\frac{1}{x}}{h}} && \hspace{2cm} \text{définition de } f   && \hspace{2cm}   \text{F.I. } && \frac{0}{0} \\[0.3cm]
=& \ \dlim{h \to 0}{\paren*{\frac{x-(x+h)}{x(x+h)}\cdot \frac{1}{h}}}   \\[0.3cm]
=& \ \dlim{h \to 0}{\frac{-h}{x(x+h)h}}   \\[0.3cm]
=& \ \dlim{h \to 0}{\frac{-1}{x(x+h)}}   \\[0.3cm]
=& \ -\frac{1}{x^2}
\end{alignat*}
}
\end{exemple}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Trouver la dérivée de la fonction $f(x)=\displaystyle \sqrt{x}$ en utilisant la définition de la dérivée.
}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{4}
 f'(x) =& \ \dlim{h \to 0}{\frac{f(x+h) - f(x)}{h}}  &&\hspace{2cm}   \text{\infobulle{définition de la dérivée}{defDerivee}} \\[0.3cm]
=& \ \dlim{h \to 0}{\displaystyle \frac{\sqrt{x+h}-\sqrt{x}}{h}} && \hspace{2cm} \text{définition de } f   &&    \text{F.I. } && \frac{0}{0} \\[0.3cm]
=& \ \dlim{h \to 0}{\paren*{\frac{\sqrt{x+h}-\sqrt{x}}{h}\cdot \frac{\sqrt{x+h}+\sqrt{x}}{\sqrt{x+h}+\sqrt{x}}}}   \\[0.3cm]
=& \ \dlim{h \to 0}{\frac{x+h-x}{h(\sqrt{x+h}+\sqrt{x})}}   \\[0.3cm]
=& \ \dlim{h \to 0}{\frac{1}{\sqrt{x+h}+\sqrt{x}}}   \\[0.3cm]
=& \ \frac{1}{2\sqrt{x}}
\end{alignat*}
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{b}[Dérivée]

\onslide<+-> \alert<1>{Qu'est-ce qu'une dérivée?    \\[0.3cm]}

\onslide<+-> \alert<2>{Interprétation de la dérivée graphiquement. \\[0.3cm]}

\onslide<+-> \alert<3>{Lien entre position, vitesse et accélération. \\[0.3cm] }

\onslide<+-> \alert<4>{\infobulle{Définition de la dérivée}{defDeriveepoint}. \\[0.3cm]}

\onslide<+-> \alert<5>{Continuité et dérivabilité}
\end{bloc}%b
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Trouver la dérivée de $f(x) = \dfrac{1}{\sqrt{x+1}}$.}{$f'(x) = -\dfrac{1}{2(x+1)^{\frac{3}{2}}}$}[\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{eim} 
\begin{secExercices}
\Q{Déterminer, à l'aide de la définition de la dérivée, la dérivée des fonctions suivantes.}
\debutSousQ
\QR{$f(x)=\dfrac{1}{\sqrt{x+2}}$}{$f'(x)=-\dfrac{1}{2\sqrt{(x+2)^3}}$}
\QR{$f(x)=\dfrac{2}{x^2+x}$}{$f'(x)=-\dfrac{2(2x+1)}{(x^2+x)^2}$} 
\QR{$f(x)=-x^2+x-1$}{$f'(x)={-2}x+1$} 
\QR{$f(x)=\sqrt{x^2+3}$}{$f'(x)=\dfrac{x}{\sqrt{x^2+3}}$}  
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Règles de dérivation}
%
%
\begin{bloc}{bm}
De la même façon que nous l'avons fait dans la partie sur les polynômes, nous aimerions trouver des formules nous permettant d'éviter de devoir utiliser les limites chaque fois que nous voulons calculer une dérivée. Les formules vues précédemment (\infobulle{théorème \ref{thmDeriveeConstante}}{thmDeriveeConstante}, 
\infobulle{théorème \ref{thmDeriveeMultiplicationConstante}}{thmDeriveeMultiplicationConstante},
\infobulle{théorème \ref{thmDeriveeAddition}}{thmDeriveeAddition}, 
\infobulle{théorème \ref{thmDeriveeMultiplication}}{thmDeriveeMultiplication} et
\infobulle{théorème \ref{thmDeriveeChaine}}{thmDeriveeChaine}) sont vraies pour toutes les fonctions, pas seulement pour les polynômes. À celles-ci s'ajoutent les deux formules des théorèmes \ref{thmDeriveeQuotient} et \ref{thmDeriveePuissanceNreel}. 
\end{bloc}%bm
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée d'un quotient]{thmDeriveeQuotient}
Si $f(x) = \displaystyle \frac{g(x)}{h(x)}$ où $g$ et $h$ sont des fonctions dérivables et $h(x)\neq 0$, alors%
\[f'(x) = \displaystyle \frac{ g'(x) \, h(x) - g(x)\, h'(x)}{(h(x))^2} \text{ .}\] \index{Théorème!dérivée d'un quotient} 

\preuve{ 
\begin{alignat*}{4}
 f'(x)  =& \ \dlim{\Delta x \to 0}{\frac{f(x+\Delta x) - f(x)}{\Delta x}}  \hspace{3cm}   \text{\infobulle{définition de la dérivée}{defDerivee}} \\[0.6cm]
=& \ \dlim{\Delta x \to 0}{\frac{\displaystyle\frac{g(x+\Delta x)}{h(x+\Delta x)}-\frac{g(x)}{h(x)}}{\Delta x}}  \hspace{3cm} \text{définition de } f    \hspace{2cm}   \text{F.I. }  \frac{0}{0} \\[0.6cm]
=& \ \dlim{\Delta x \to 0}{\frac{g(x+\Delta x)h(x)-g(x)h(x+\Delta x)}{\Delta x\, h(x)\, h(x+\Delta x)}} \\[0.6cm]
&    \hspace{1cm}   \text{Nous allons additionner 0 au numérateur.} \\[0.6cm]
=& \ \dlim{\Delta x \to 0}{\frac{g(x+\Delta x)h(x){\color{red}-g(x)h(x)+g(x)h(x)}-g(x)h(x+\Delta x)}{\Delta x\, h(x)\, h(x+\Delta x)}} \\[0.6cm]
=& \ \dlim{\Delta x \to 0}{\paren*{\paren*{\frac{h(x)\left[g(x+\Delta x)-g(x)\right]}{\Delta x}-\frac{g(x)\left[h(x+\Delta x)-h( x)\right]}{\Delta x}}\cdot\frac{1}{h(x)\, h(x+\Delta x)}}} \\
\end{alignat*}

\newpage

\begin{alignat*}{4}
 &    \hspace{1cm}   \text{Nous allons utiliser les propriétés 1 et 2 du \infobulle{théorème \ref{thmLimiteProp}}{thmLimiteProp}.} \\[0.6cm]
f'(x) =& \paren*{\dlim{\Delta x \to 0}{\frac{h(x)\left[g(x+\Delta x)-g(x)\right]}{\Delta x}}-\dlim{\Delta x \to 0}{\frac{g(x)\left[h(x+\Delta x)-h(x)\right]}{\Delta x}}}\cdot\dlim{\Delta x \to 0}{\frac{1}{h(x)\, h(x+\Delta x)}} \\[0.6cm]
&    \hspace{1cm}   \text{Nous allons utiliser les propriétés 2 et 3 du \infobulle{théorème \ref{thmLimiteProp}}{thmLimiteProp}.} \\[0.6cm]
=& \ \paren*{h(x) \dlim{\Delta x \to 0}{\frac{g(x+\Delta x)-g(x)}{\Delta x}}-g(x) \dlim{\Delta x \to 0}{\frac{h(x+\Delta x)-h(x)}{\Delta x}}}\cdot\frac{1}{\dlim{\Delta x \to 0}{h(x)\, h(x+\Delta x)}} \\[0.6cm]
=& \ \paren*{ h(x)\, g'(x)-g(x)\, h'(x)}\cdot\frac{1}{\paren*{h(x) }^2}\hspace{2cm}   \text{\infobulle{définition de la dérivée}{defDerivee}} \\[0.6cm]
=& \ \displaystyle \frac{ g'(x) \, h(x) - g(x)\, h'(x)}{(h(x))^2} 
\end{alignat*}
\finDemo 
}
\end{thm}
\dans{n}{\vfill}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Dériver la fonction $h(t)=\displaystyle \frac{3t^2-4t+2}{1-2t}$.
}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{4}
 h'(t) =& \ \displaystyle \frac{(3t^2-4t+2)'(1-2t)-(3t^2-4t+2)(1-2t)'}{(1-2t)^2} && \hspace{2cm} \text{\infobulle{théorème \ref{thmDeriveeQuotient}}{thmDeriveeQuotient}}\\[0.3cm]
=& \  \displaystyle \frac{(6t-4)(1-2t)-(3t^2-4t+2)(-2)}{(1-2t)^2}\\[0.3cm]
=& \  \displaystyle \frac{6t-12t^2-4+8t+6t^2-8t+4}{(1-2t)^2}\\[0.3cm]
=& \  \displaystyle \frac{{-6}t^2+6t}{(1-2t)^2}\\[0.3cm]
=& \  \displaystyle \frac{{-6}t(t-1)}{(1-2t)^2}
\end{alignat*}
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Trouver la dérivée de $f(x) = \dfrac{x+1}{x^2-3}$.}{$f'(x) = -\dfrac{x^2+2x+3}{(x^2-3)^2}$}[\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bmn}
\begin{attention}{attDeriveeQuotient}
La dérivée d'un quotient de fonctions n'est pas égale au quotient des dérivées de ces fonctions.%
\[\paren*{\displaystyle \frac{g(x)}{h(x)}}' \neq \frac{g'(x)}{h'(x)}\]
\end{attention}
\dans{n}{\newpage}
\end{bloc}%bmn
%
%
\begin{bloc}{m}
Le \infobulle{théorème \ref{thmDeriveePuissanceDeX}}{thmDeriveePuissanceDeX} généralisé aux nombres réels va nous permettre de dériver les fonctions contenant des puissances de $x$ négatives et les fonctions contenant des racines.
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{thm}[Dérivée d'une puissance avec\dans{i}{\newline} exposant réel]{thmDeriveePuissanceNreel}
Si $f(x) = x^k$, où $k \in \mathds{R}$, alors $f'(x) = kx^{k-1}$. \index{Théorème!dérivée d'une puissance réelle de $x$} 
\end{thm}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
La démonstration de ce théorème sera faite dans la partie \ref{partie3} qui porte sur les fonctions transcendantes.

\begin{exemple}
\QS{Dériver la fonction $f(x)=\displaystyle \frac{1}{2x}+\frac{5}{\sqrt[3]{x}}-\sqrt[5]{x^2}+\frac{\pi}{2}-\sqrt{e}$.
}{
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{4}
 f'(x) =& \ \displaystyle \paren*{\frac{1}{2}x^{-1}}'+\paren*{5x^{-1/3}}'-\paren*{x^{2/5}}'+\paren*{\frac{\pi}{2}}'-\paren*{\sqrt{e}}' \\[0.3cm]
=& \ \displaystyle \frac{1}{2}\paren*{x^{-1}}'+5\paren*{x^{-1/3}}'-\paren*{x^{2/5}}'+0-0   \\[0.3cm]
=& \ \displaystyle \frac{1}{2}\cdot (-1)x^{-2}+5\cdot \frac{-1}{3}x^{-4/3}-\frac{2}{5}x^{-3/5}   \\[0.3cm]
=& \ \displaystyle -\frac{1}{2x^2}-\frac{5}{3x^{4/3}}-\frac{2}{5x^{3/5}}  
\end{alignat*}
}
\end{exemple}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Dériver la fonction $f(x)=\displaystyle \sqrt{\frac{2x+1}{x^2+3}}$.
}{

Il s'agit ici d'une dérivée en chaîne dont la forme de base est une racine. On peut réécrire la fonction $f$ de la façon suivante: $f(x)=\displaystyle \paren*{\frac{2x+1}{x^2+3}}^{1/2}$.
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{4}
 f'(x) =& \ \displaystyle \frac{1}{2}\paren*{\frac{2x+1}{x^2+3}}^{-1/2}\cdot \paren*{\frac{2x+1}{x^2+3}}' \\[0.3cm]
=& \ \displaystyle \frac{1}{2}\paren*{\frac{2x+1}{x^2+3}}^{-1/2}\cdot \frac{(2x+1)'(x^2+3)-(2x+1)(x^2+3)'}{(x^2+3)^2} \\[0.3cm]
=& \ \displaystyle \frac{1}{2}\paren*{\frac{2x+1}{x^2+3}}^{-1/2}\cdot \frac{2(x^2+3)-(2x+1)\cdot 2x}{(x^2+3)^2} \\[0.3cm]
=& \ \displaystyle \frac{\sqrt{x^2+3}}{2\sqrt{2x+1}}\cdot \frac{{-2}x^2-2x+6}{(x^2+3)^2} \\[0.3cm]
=& \ \displaystyle \frac{{-2}(x^2+x-3)}{2\sqrt{2x+1}(x^2+3)^{3/2}}\\[0.3cm]
=& \ \displaystyle \frac{-x^2-x+3}{\sqrt{2x+1}\sqrt{(x^2+3)^3}}
\end{alignat*}
}
\end{exemple}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Déterminer $\displaystyle \frac{\dd y}{\dd x}$ si $\sqrt{x+y}+2xy=3x-y$.
}{

Il s'agit d'une équation implicite. Il faut d'abord dériver chaque côté de l'équation par rapport à $x$ et ensuite isoler la dérivée recherchée.
\setlength{\belowdisplayskip}{0pt}%
\begin{alignat*}{4}
&\frac{\dd }{\dd x}\sqrt{x+y}+\frac{\dd }{\dd x}(2xy) =\frac{\dd }{\dd x}(3x) - \frac{\dd }{\dd x}y\\[0.3cm]
\Rightarrow & \ \frac{1}{2}(x+y)^{-1/2}\cdot \frac{\dd }{\dd x}(x+y)+2\paren*{y+x\frac{\dd y}{\dd x}}=3-\frac{\dd y}{\dd x}\\[0.3cm]
\Rightarrow &\  \frac{1}{2\sqrt{x+y}}\paren*{1+\frac{\dd y}{\dd x}}+2y+2x\cdot \frac{\dd y}{\dd x} =3-\frac{\dd y}{\dd x}\\[0.3cm]
\Rightarrow &\  \frac{1}{2\sqrt{x+y}}\cdot\frac{\dd y}{\dd x}+2x\cdot \frac{\dd y}{\dd x}+\frac{\dd y}{\dd x} =3-2y-\frac{1}{2\sqrt{x+y}}\\[0.3cm]
\Rightarrow &\ \frac{\dd y}{\dd x}\paren*{ \frac{1}{2\sqrt{x+y}}+2x+1} =3-2y-\frac{1}{2\sqrt{x+y}}\\[0.3cm]
\Rightarrow &\ \frac{\dd y}{\dd x} =\frac{3-2y-\displaystyle \frac{1}{2\sqrt{x+y}}}{\displaystyle \frac{1}{2\sqrt{x+y}}+2x+1}\\[0.3cm]
\Rightarrow &\ \frac{\dd y}{\dd x} =\frac{6\sqrt{x+y}-4y\sqrt{x+y}-1}{ 2\sqrt{x+y}}\cdot \frac{2\sqrt{x+y}}{2\sqrt{x+y}+4x\sqrt{x+y}+1}\\[0.3cm]
\Rightarrow &\ \frac{\dd y}{\dd x} = \frac{6\sqrt{x+y}-4y\sqrt{x+y}-1}{2\sqrt{x+y}+4x\sqrt{x+y}+1}
\end{alignat*}
}
\end{exemple}
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Trouver la pente de la droite tangente à la courbe décrite par l'équation implicite \newline $x^{2/3}+y^{2/3}=4$ au point $(1, {-3}\sqrt{3})$.
}{

Il faut d'abord dériver chaque côté de l'équation par rapport à la variable $x$. et ensuite isoler la dérivée recherchée.
\begin{alignat*}{4}
&\frac{\dd}{\dd x}x^{2/3}+\frac{\dd }{\dd x}y^{2/3} =\frac{\dd }{\dd x}4\\[0.3cm]
\Rightarrow & \ \frac{2}{3}x^{-1/3}+\frac{2}{3}y^{-1/3}\cdot \frac{\dd y}{\dd x}=0\\[0.3cm]
\Rightarrow &\  \frac{2}{3}y^{-1/3}\cdot \frac{\dd y}{\dd x} =-\frac{2}{3}x^{-1/3}\\[0.3cm]
\Rightarrow & \ \frac{\dd y}{\dd x} =\displaystyle \frac{-\displaystyle \frac{2}{3}x^{-1/3}}{\displaystyle \frac{2}{3}y^{-1/3}}\\[0.3cm]
\Rightarrow & \  \frac{\dd y}{\dd x} =-\frac{y^{1/3}}{x^{1/3}}
\end{alignat*}

Comme on veut la pente au point $(1, {-3}\sqrt{3})$, il suffit de remplacer et on obtient
\[\left. \frac{\dd y}{\dd x}\right|_{(1, {-3}\sqrt{3})} =-\displaystyle \frac{({-3}\sqrt{3})^{1/3}}{1^{1/3}}= (3\sqrt{3})^{1/3}= \sqrt{3}\quad .\]

La figure suivante montre la courbe décrite par l'équation $x^{2/3}+y^{2/3}=4$ et nous venons de déterminer la pente de la droite tangente au point $(1, {-3}\sqrt{3})$ représentée en rouge.
\begin{center}
\includegraphics[scale=0.7]{Tikz/Images/chap_8_ex_implicite_1_man}
\end{center}
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Trouver la dérivée de $f(x) = \sqrt{x}$.}{$f'(x) = \dfrac{1}{2\sqrt{x}}$}[\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}[allowframebreaks]{bn}
\begin{exercice}

\Q{Trouver les dérivées des fonctions suivantes.}
\debutSousQ[itSep=0.3cm, nbCols=2]
\QR{$f(x) = 3x^{\frac{2}{3}}+\dfrac{4}{x^2} - \sqrt[3]{4x} + \sqrt{17}$}{$f'(x) =  \dfrac{2}{x^{\frac{1}{3}}} - \dfrac{8}{x^3} - \dfrac{4}{3(4x)^{\frac{2}{3}}}$}
\QR{$f(x) = \dfrac{3x^2-x}{x^3+1}$}{$f'(x) = \dfrac{(6x-1)(x^3+1)-3x^2(3x^2-x)}{(x^3+1)^2}$}
\framebreak
\QR{$f(x) = \sqrt{\dfrac{x-1}{x+1}}$}{$f'(x) = \dfrac{1}{\sqrt{x-1}(x+1)^{\frac{3}{2}}}$}
\QR{$f(x) = (4x^2+2x)\sqrt{5x^3-1}$}{$f'(x) = (8x+2)\sqrt{5x^3-1}+\dfrac{15x^2(2x^2+x)}{\sqrt{5x^3-1}}$}
\framebreak
\QR{$f(x) = \sqrt[5]{(4x^3+2x-1)^8}$}{$f'(x) = \frac{8}{5}(4x^3+2x-1)^{\frac{3}{5}}(12x^2+2)$}
\QR{$f(x) = \dfrac{({-3}x^6+x^4)^5}{(x^4-3x^6)^3}$}{$f'(x) = 4({-3}x^6+x^4)({-9}x^2+2x^3)$}
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}
\debutSousQ[itSep=0.3cm]
\QR{Trouver $\dfrac{\dd y}{\dd x}$ si $\dfrac{x}{x+y}=2xy$.}{$\dfrac{\dd y}{\dd x} = \dfrac{y-2y(x+y)^2}{2x(x+y)^2+x}$} 

\QR{Déterminer l'équation de la droite tangente à la courbe d'équation $f(x) = \dfrac{\sqrt{x}}{x+1}$ au point $(1, \frac{1}{2})$.}{$y = \dfrac{1}{2}$} 
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?time_continue=1&v=tNtGjXYbEN8}{Dérivée d'un quotient (démonstration différente de celle du manuel et exemple).}

\href{https://www.youtube.com/watch?v=_OdxDabOHds}{Exemple de calcul du TVI avec la définition ($h\to 0$). À regarder à partir de 3 minutes et 32 secondes.}

\href{https://www.wolframalpha.com}{\textit{WolframAlpha}}

Voici un exemple d'écriture de dérivée dans un langage que \textit{WolframAlpha} comprend. 

Pour calculer $\dfrac{\dd }{\dd x}3x^2$, on écrit \og d/dx 3x\^{}2\fg{} ou encore \og derive 3x\^{}2 \fg{}. 
\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Déterminer la dérivée des fonctions suivantes.}
\debutSousQ
\QR{$f(x)=\sqrt{x}-2x^2+\dfrac{4}{\sqrt{x}}$}{$f'(x)=\dfrac{1}{2\sqrt{x}}-4x-\dfrac{2}{\sqrt{x^3}}$}
\QR{$f(x)=\sqrt{2x^{\frac{3}{4}}-5}$}{$f'(x)=\dfrac{3}{4\sqrt[4]{x}\sqrt{2x^{\frac{3}{4}}-5}}$} 
\QR{$f(x)=(x^2+x+1)^{\frac{7}{4}}$}{$f'(x)=\dfrac{7}{4}(x^2+x+1)^{\frac{3}{4}}(2x+1)$}
\QR{$f(x)=\dfrac{7}{x^2-3}$}{$f'(x)=-\dfrac{14x}{(x^2-3)^2}$}  
\QR{$f(x)=\dfrac{4x+1}{x^2-5}$}{$f'(x)=\dfrac{4(x^2-5)-2x(4x+1)}{(x^2-5)^2}$}  
\QR{$f(x)=\dfrac{5}{8-\sqrt[5]{x}}$}{$f'(x)=\dfrac{1}{\sqrt[5]{x^4}(8-\sqrt[5]{x})^2}$} 
\QR{$f(x)=\dfrac{2-x}{-x+4}$}{$f'(x)=-\dfrac{2}{(4-x)^2}$}  
\QR{$f(x)=\dfrac{(x+2)^3}{(x-4)^4}$}{\begin{align*}
f'(x)&=\dfrac{3(x+2)^2(x-4)-4(x+2)^3}{(x-4)^5} \\ &= -\dfrac{(x+2)^2(20+x)}{(x-4)^5}
\end{align*}
}[6.5cm]  
\QR{$f(x)=\dfrac{(2x-7)(x+2)}{4-x}$}{$f'(x)=\dfrac{(4x-3)(4-x)+(2x-7)(x+2)}{(4-x)^2}$}  
\finSousQ

\Q{Déterminer $\dfrac{\textrm{d}y}{\textrm{d}x}$.}

\debutSousQ  
\QR{$y=\sqrt{x^4-x^7}\sqrt{x^2-3}$}{$\dfrac{\textrm{d}y}{\textrm{d}x}=\dfrac{(4x^3-7x^6)\sqrt{x^2-3}}{2\sqrt{x^4-x^7}} +\dfrac{x\sqrt{x^4-x^7}}{\sqrt{x^2-3}}$}
\QR{$x-y=\sqrt{xy}$}{$\dfrac{\textrm{d}y}{\textrm{d}x}=\dfrac{2\sqrt{xy}-y}{2\sqrt{xy}+x}$}%
\QR{$\sqrt[3]{x^2}+y^2=\dfrac{x}{y}$}{$\dfrac{\textrm{d}y}{\textrm{d}x}=\dfrac{3y\sqrt[3]{x}-2y^2}{3\sqrt[3]{x}(x+2y^3)}$}
\QR{$(x+y)^{\frac{5}{3}}=3y^4-2x+1$}{$\dfrac{\textrm{d}y}{\textrm{d}x}=\dfrac{5\sqrt[3]{(x+y)^2}+6}{36y^3-5\sqrt[3]{(x+y)^2}}$}
\finSousQ

\Q{Trouver toutes les valeurs de $x$ où la tangente à la courbe $y=f(x)$ donnée est horizontale.}

\debutSousQ
\QR{$y=\dfrac{x^2-1}{x+2}$}{$x={-2}+\sqrt{3}$ et $x={-2}-\sqrt{3}$}
\QR{$y=\dfrac{x+2}{x+3}$}{Il n'y a aucune valeur de $x$ telle que\dans{m}{\\} $y'(x)=0$.}
\finSousQ

\dans{m}{\columnbreak}

\Q{Déterminer si possible $F'(3)$, sachant que $f(3)={-1}$, $f'(3)=1$, $f'(\frac{1}{3})=9$, $g(3)=3$, $g'(3)=2$.}

\debutSousQ
\QR{$F(x)=f(x)g(x)$}{$F'(3)=1$}
\QR{$F(x)=3f(x)-g(x)$}{$F'(3)=1$}
\QR{$F(x)=\dfrac{x}{g(x)}$}{$F'(3)=-\dfrac{1}{3}$}
\QR{$F(x)=f\paren*{\dfrac{1}{g(x)}}$}{$F'(3)={-2}$}
\QR{$F(x)=\dfrac{\paren*{f(x)}^2}{g(x)}$}{$F'(3)=-\dfrac{8}{9}$}
\finSousQ

\Q{Donner l'équation de la tangente à la courbe $f(x)$ en $x=a$.}

\debutSousQ
\QR{$f(x)=\sqrt[4]{2x^3-1}$ et $a=1$}{$y=\frac{3}{2}x-\frac{1}{2}$}
\QR{$f(x)=\dfrac{(4-3x^2)^2}{x-1}$ et $a=-1$}{$y=-\frac{25}{4}x-\frac{27}{4}$}
\finSousQ
\end{secExercices}
\end{bloc}%eim
