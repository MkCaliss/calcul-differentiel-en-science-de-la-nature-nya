%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\part{Les fonctions algébriques} \label{partie2}
%
%
\chapter{Notions préalables}
%
%
\section{Fonctions rationnelles}
%
%
\begin{bloc}{m}
Nous commencerons ce chapitre en présentant un cas particulier de fonction algébrique : les fonctions rationnelles.
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{defi}[Fonction rationnelle]{defRationnelle}
Soit $p(x)$ et $q(x)$ deux polynômes. Alors la fonction \newline $f(x)=\dfrac{p(x)}{q(x)}$, avec $q(x) \neq 0$, est appelée \defTerme{fonction rationnelle}.\index{Fonction!rationnelle}
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bmn}
Une fonction rationnelle bien connue est la fonction de variation inverse $f(x)=\frac{1}{x}$ dont la représentation graphique est la suivante.
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_6_fct_rationnelle_1_bmn.pdf}
\end{center}
\end{bloc}%bmn
%
%
\begin{bloc}{m}
On voit aisément que le domaine de cette fonction est $\mathds{R} \setminus \{0\} $.  

Les fonctions rationnelles peuvent toutefois avoir des représentations graphiques de formes très variées. La figure suivante en présente quelques exemples. 
\dans{m}{\newpage}
\begin{figure}[!h]
\begin{multicols}{3}
\centering
$f(x)=\dfrac{x^3+1}{x^3-1}$ \\
\includegraphics[scale=1]{Tikz/Images/chap_6_ex_fct_rationnelle_2_man.pdf}

\columnbreak

$g(x)=\dfrac{x^2+2x}{x^2-1}$ \\
\includegraphics[scale=1]{Tikz/Images/chap_6_ex_fct_rationnelle_3_man.pdf}

\columnbreak

$h(x)=\dfrac{x^2}{x^2-x-2}$ \\
\includegraphics[scale=1]{Tikz/Images/chap_6_ex_fct_rationnelle_4_man.pdf}
\end{multicols}
\caption{Exemples de fonctions rationnelles}
\end{figure}
\end{bloc}%m
%
%
\dans{n}{\stepcounter{figure}}
%
%
\begin{bloc}{b}[Exemple de fonction rationnelle]
\begin{center}
$f(x)=\dfrac{x^3+1}{x^3-1}$ \\[0.5cm]
\includegraphics[scale=1]{Tikz/Images/chap_6_ex_fct_rationnelle_5_bea.pdf}
\end{center}
\end{bloc}%b
%
%
\begin{bloc}{b}[Exemple de fonction rationnelle]
\begin{center}
$h(x)=\dfrac{x^2}{x^2-x-2}$ \\[0.5cm]
\includegraphics[scale=1]{Tikz/Images/chap_6_ex_fct_rationnelle_6_bea.pdf}
\end{center}
\end{bloc}%b
%
%
\begin{bloc}{m}

À la fin de la \autoref{partie2} de ce manuel, vous serez en mesure de trouver la représentation graphique de diverses fonctions rationnelles.
\end{bloc}%m
%
%
\subsubsection{Mettre au même dénominateur}
%
%
\begin{bloc}{m}
L'addition et la soustraction de fractions nécessitent que celles-ci aient le même dénominateur. Il est généralement recommandé d'utiliser le plus petit dénominateur commun afin de faciliter le travail de simplification par la suite. 

Le plus petit dénominateur commun n'est rien d'autre que le plus petit commun multiple des dénominateurs. 
\end{bloc}%m
%
%
\begin{bloc}{bn}[Opérations sur les fractions algébriques]
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item Multiplication
\item Division
\item Addition et soustraction
\end{liste}
\end{bloc}%bn
%
%
\begin{bloc}{bimn}
\begin{defi}[Plus petit commun multiple]{defPPCM}
Le \defTerme{plus petit commun multiple} (PPCM) de deux ou de plusieurs entiers est le plus petit nombre qui est un multiple de chacun de ces nombres.\index{Plus petit commun multiple}
\end{defi}
\end{bloc}%bimn
%
%
\begin{bloc}{bmn}[Pour trouver le PPCM]
\dans{m}{\textbf{Pour trouver le PPCM de deux nombres entiers :}}
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item On décompose chacun des nombres en facteurs premiers. 
\item Le PPCM est le produit de tous les facteurs figurant dans l'une ou l'autre des décompositions, chaque facteur étant pris avec son plus grand exposant.
\end{liste}
\end{bloc}%bmn   
%
%
\begin{bloc}{m}
\dans{m}{\newpage}

\begin{exemple}
\QS{Déterminer le plus petit commun multiple (PPCM) de 108 et 420.  
}{

Il faut d'abord décomposer ces deux nombres en facteurs premiers.%
\[108 = 2^2 \cdot 3^3 \textrm{ et } 420 = 2^2 \cdot 3 \cdot 5 \cdot 7\]
Le PPCM de 108 et 420 est donc $2^2 \cdot 3^3 \cdot 5 \cdot 7 = \num{3780}.$ Ainsi, si on devait additionner $\frac{1}{108}$ et $\frac{1}{420}$, le plus petit dénominateur commun à utiliser serait \num{3780}.
}
\end{exemple}  

On peut appliquer un raisonnement similaire à des expressions algébriques.

\begin{exemple}
\QS{Effectuer $\paren*{\dfrac{x}{x^2-9}+\dfrac{x+1}{x^3+2x^2-3x}}$.  
}{

Il faut d'abord décomposer les deux dénominateurs en facteurs.%
\[x^2-9 = (x+3)(x-3) \textrm{   et   } x^3+2x^2-3x = x(x+3)(x-1)\]
Le PPCM de $x^2-9$ et $x^3+2x^2-3x$ est donc $x(x+3)(x-3)(x-1)$. On obtient:
\begin{center}
$ \displaystyle
    \begin{aligned}  
\dfrac{x}{x^2-9}+\dfrac{x+1}{x^3+2x^2-3x} &= \dfrac{x}{(x+3)(x-3)}+\dfrac{x+1}{x(x+3)(x-1)} \\[0.4cm]
            &= \dfrac{x^2(x-1)+(x+1)(x-3)}{x(x+3)(x-3)(x-1)} \\[0.4cm]
            &= \dfrac{x^3-2x-3}{x(x+3)(x-3)(x-1)} 
\end{aligned}
$  
\end{center}
}
\end{exemple} 
\dans{ordi}{\newpage}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exemple}
\debutSousQ[itSep=0.3cm, beaOpt=<+- |alert@+>]
\QR{Effectuer $\dfrac{11}{300} + \dfrac{5}{126}$.}{$\dfrac{481}{\num{6300}}$}[\vfill]
\QR{Effectuer $\dfrac{1}{x^2+3x+2}+\dfrac{x-1}{(x+3)(x+1)^2}$.}{$\dfrac{2x^2+5x+1}{(x +1)^2 (x+2)(x+3)}$}[\vfill \newpage]
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\section{Fonctions algébriques}
%
%
\begin{bloc}{bimn}
\begin{defi}[Fonction algébrique]{defFctAlgebrique}
Une fonction est appelée \defTerme{fonction algébrique} lorsque celle-ci contient un nombre fini d'opérations algébriques (addition, soustraction, multiplication, division et extraction de racine) appliquées à des polynômes.\index{Fonction!algébrique}
\end{defi}
\end{bloc}%bimn
%
%
\subsection{Fonction racine carrée}
%
%
\begin{bloc}{m}
Un exemple important de fonction algébrique qui n'est pas une fonction rationnelle est la fonction racine carrée. La figure suivante présente la fonction $f(x)=\sqrt{x}$. Son domaine est $[0, \infty[$. 
\end{bloc}%m
%
%
\begin{bloc}{bmn}[\dans{b}{Fonction racine carrée}]
\dans{mn}{\begin{figure}[!h]}
\centering
\includegraphics[scale=1]{Tikz/Images/chap_6_fct_racine_7_bmn.pdf}
\dans{mn}{
\caption{Fonction racine carrée}
\end{figure}}
\dans{mob}{\newpage}
\end{bloc}%bmn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Est-ce que $\sqrt{9} = \pm 3$?}{Non, $\sqrt{9} = 3$. L'équation $x^2=9$ a comme solution $x = \pm 3$.}[\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\subsection{Domaine}
%
%
\begin{bloc}{m}
On se rappelle que, pour déterminer le \infobulle{domaine}{defDomaine} d'une fonction, il suffit de retirer de l'ensemble des nombres réels les nombres qui engendrent des calculs impossibles. Voyons comment il peut être impossible d'évaluer $f(x)$ lorsque $f$ est une fonction algébrique. 
\end{bloc}%m
%
%
\begin{bloc}{b}[Domaine]
\begin{center}
\infobulle{Domaine}{defDomaine} d'une fonction.
\end{center}
\end{bloc}%b
%
%
\begin{bloc}{bimn}[\dans{b}{Calculs impossibles}] 
\creerInfobulle[10cm]{
\dans{i}{
\begin{center}
Calculs impossibles
\end{center}
}
\dans{mn}{\begin{table}[!h] 
\caption{Calculs impossibles}  
\label{tabCalculsImpossibles}} 
\begin{cadre}[8cm]{c}
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]  
\item Division par zéro  
\item Racine paire d'un nombre strictement négatif    
\end{liste}
\end{cadre}
\dans{mn}{\end{table}}
}{tabCalculsImpossibles}
\dans{n}{\newpage}
\end{bloc}%bimn


\begin{bloc}{m}
Remarquons que les racines impaires ne posent pas de problème de calcul. 
\dans{ordi}{\newpage} 

\begin{exemple}
\QS{Déterminer le domaine de la fonction $f(x) = \dfrac{x^2-1}{x-1}$. 
}{

Il n'y a pas de racine dans le calcul de $f$. Cependant, il y a une division. Nous devons vérifier si le dénominateur peut être nul.%
\[x-1 = 0 \Rightarrow x = 1\] 
Ainsi, la fonction n'est pas définie en $x=1$, alors $\dom(f) = \mathds{R} \setminus \{1\}$.
}
\end{exemple}  

\begin{exemple}
\QS{Déterminer le domaine de la fonction $f(x) = \dfrac{x^2-1}{x^2+4}$. 
}{

Il n'y a pas de racine dans le calcul de $f$. Cependant, il y a une division. Nous devons vérifier si le dénominateur peut être nul. 
\[x^2+4 = 0 \Rightarrow x^2 = {-4}\] 
Ceci est impossible dans les nombres réels, alors $\dom(f) = \mathds{R}$.
}
\end{exemple}  

\begin{exemple}
\QS{Déterminer le domaine de la fonction $f(x) = \sqrt{x+2}$. 
}{

Il n'y a pas de division dans le calcul de $f$. Cependant, il y a une racine carrée. Nous devons vérifier si le radicande peut être négatif.% 
\[x+2 < 0 \Rightarrow x < {-2}\]
Ainsi, la fonction n'est pas définie pour toutes les valeurs inférieures à ${-2}$, alors $\dom(f) = [{-2}, \infty[$. On remarque l'inclusion de la valeur $2$ puisque la racine carrée de zéro existe.
}
\end{exemple} 

\begin{exemple}
\QS{Déterminer le domaine de la fonction $f(x) = \sqrt[5]{x-8}$. 
}{

Il n'y a pas de division dans le calcul de $f$. Cependant, il y a une racine, mais puisque celle-ci est impaire, on a $\dom(f) = \mathds{R}$.
}
\end{exemple}  

\begin{exemple}
\QS{Déterminer le domaine de la fonction $f(x) = \dfrac{\sqrt[6]{(5-x)}}{\sqrt{x^2-9} - 1}$. 
}{

Étudions d'abord la racine sixième. $5-x < 0 \Rightarrow x > 5$. Ainsi, la fonction n'existe pas si $x$ est supérieur à $5$. 

Voyons ensuite la racine carrée. $x^2-9 < 0 \Rightarrow x\in ]{-3}, 3[$. Ainsi, la fonction n'existe pas si $x$ est entre ${-3}$ et $3$. 

Enfin, regardons la division. $\sqrt{x^2-9} - 1 = 0 \Rightarrow x= \pm \sqrt{10}$. Ainsi, la fonction n'existe pas à ces deux valeurs. 

Finalement, nous trouvons 

$\begin{aligned} \dom(f) &= \Big(\paren[\big]{\mathds{R} \ \setminus \ \left] 5, \infty \right[ } \ \setminus \ \left]{-3}, 3 \right[ \Big)\ \setminus \ \lbrace -\sqrt{10}, \sqrt{10}\rbrace  \\
 &= \left]{-\infty}, -\sqrt{10}\right[ \ \cup \ \left]-\sqrt{10}, {-3} \right] \ \cup \ \left[3, \sqrt{10} \right[ \ \cup \ \left]\sqrt{10}, 5 \right] \text{ .}\end{aligned}$ 
}
\end{exemple} 
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Déterminer le domaine de la fonction $f(x) = \dfrac{x+2}{(x-3)(5+x)}$.  
}{

Il n'y a pas de racine dans le calcul de $f$. Cependant, il y a une division. Nous devons vérifier si le dénominateur peut être nul.% 
\[(x-3)(5+x) = 0 \Rightarrow x = {-5} \textrm{ ou } x = 3\] 
Ainsi, la fonction n'est pas définie en $x=-{5}$ et en $x=3$, alors $\dom(f) = \mathds{R} \setminus \{{-5}, 3\}$.
}
\end{exemple}  

\begin{exemple}
\QS{Déterminer le domaine de la fonction $f(x) = \sqrt{\dfrac{x+3}{2-x}}$. 
}{

Il y a une division dans le calcul de $f$. Nous devons vérifier si le dénominateur peut être nul.% 
\[2-x = 0 \Rightarrow x = 2\] 
Ainsi, $x=2$ ne fait pas partie du domaine de $f$. Il y a aussi une racine paire. Nous devons nous assurer que ce qui est sous la racine n'est pas négatif. Pour ce faire, nous pouvons utiliser un tableau de signes.
\begin{center}
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{|C{2cm}|C{2cm}|C{1cm}|C{2cm}|C{1cm}|C{2cm}|}
\hline
$x$ & & ${-3}$ & & $2$ & \\ \hline
$x+3$ & $-$ & $0$ & $+$ & $+$ & $+$\\ \hline
$2-x$ & $+$ & $+$ & $+$ & $0$ & $-$\\ \hline
\xrowht{26pt}$\dfrac{x+3}{2-x}$ & $-$ & $0$ & $+$ & $\nexists$ & $-$\\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
Ainsi,%
\[\dfrac{x+3}{2-x} \geq 0 \Rightarrow x \in [{-3}, 2[\]
Nous trouvons donc $\dom(f) = [{-3}, 2[$.
}
\end{exemple}  
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Déterminer le domaine de la fonction $f(x) = \dfrac{1-\sqrt{x^2-2}}{7-2x}$. 
}{

Il y a une division dans le calcul de $f$. Nous devons vérifier si le dénominateur peut être nul.% 
\[7-2x = 0 \Rightarrow x = \frac{7}{2}\] 
Ainsi, $x=7/2$ ne fait pas partie du domaine de $f$. Il y a aussi une racine paire. Nous devons nous assurer que ce qui est sous la racine n'est pas négatif.%
\[x^2-2 \geq 0 \Rightarrow x \leq -\sqrt{2} \textrm{ ou } x \geq \sqrt{2}\]
D'où on a, $\dom(f) = \crochetsof*{{-\infty}, -\sqrt{2}} \, \cup \, \crochetsfo*{\sqrt{2}, \frac{7}{2}} \, \cup \, \crochetso*{\frac{7}{2}, \infty}$.
}
\end{exemple}  
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{Déterminer le domaine des fonctions suivantes.}
\debutSousQ[beaOpt=<+- |alert@+>, itSep=0.3cm, largeurQ=4.5cm] 
\QR{$f(x) = 12x^5 - 3x^2 +7$}{$\dom(f) = \mathds{R}$}[\vspace{1cm}] 
\QR{$g(x) = \dfrac{3x^5-x}{x^2-7}$}{$\dom(g) = \mathds{R}\setminus \acco*{-\sqrt{7}, \sqrt{7}}$}[\vfill] 
\QR{$h(x) = \sqrt{x-3}$}{$\dom(h) = [3, \infty[$}[\vfill] 
\QR{$i(x) = \sqrt[3]{(x-3)}$}{$\dom(i) = \mathds{R}$}[\vfill \newpage] 
\QR[\linewidth]{$F(x) = \sqrt[4]{\dfrac{(5-x)}{(x+2)(x-1)}}$}{$\dom(F) = ]{-\infty}, -2[ \ \cup \ ]1, 5]$}[\vfill \vfill \vfill] 
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\Q{Déterminer la différence entre les fonctions données.}
\debutSousQ[itSep=0.3cm, beaOpt=<+- |alert@+>]
\QR{$f(x) = 1$ et $g(x) = \dfrac{x}{x}$}{Elles n'ont pas le même domaine.}[\vfill]
\QR{$f(x) = x$ et $g(x) = \paren[\big]{\sqrt{x}}^2$ et $h(x) = \sqrt{x^2}$}{$g$ n'a pas le même domaine que $f$ et $h$. L'image de $f$ est différente de celle des deux autres fonctions.}[\vfill]
\finSousQ
\end{exemple}
\end{bloc}%bn
%
%
\subsection{Conjugué}
%
%
\begin{bloc}{bimn}
\begin{defi}[Conjugué]{defConjugue}
Le \defTerme{conjugué} de $a \pm b$ est $a \mp b$. \index{Conjugué}
\end{defi}
\dans{n}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Déterminer le conjugué de $\sqrt{x^2-1}+4$.  
}{

On applique directement la définition. Le conjugué de $\sqrt{x^2-1} \mathbin{\textcolor{blue}{\pmb{\hm{+}}}} 4$ est $\sqrt{x^2-1} \mathbin{\textcolor{red}{\pmb{\bm{-}}}}4$.
}
\end{exemple} 
\end{bloc}%m
%
%
\begin{bloc}{bmn}
\dans{mob}{\newpage}
Un fait à noter sur les fonctions dont l'opération principale est la division est que si on cherche ses zéros (comme nous allons le faire pour trouver les endroits où il peut y avoir des extremums ou des points d'inflexion) il suffit de trouver les zéros du numérateur.\pause{} 

En effet%
\[\dfrac{a}{b} = 0 \Leftrightarrow a=0 \quad .\]
\dans{ordi}{\newpage}
\end{bloc}%bmn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Déterminer les zéros de la fonction $f(x) = \dfrac{x^2-1}{41x^{\frac{5}{3}}-12x^7+4}$.  
}{
\setlength{\belowdisplayskip}{0pt}%
\begin{align*}
& \hspace{1cm} \dfrac{x^2-1}{41x^{\frac{5}{3}}-12x^7+4} \ = \ 0 \\[0.4cm]
\Leftrightarrow &\hspace{1cm}\ x^2-1 \ = \ 0  \\[0.4cm]
\Leftrightarrow & \hspace{1cm} x=1 \text{ ou } x=-1
\end{align*}
}
\end{exemple} 
\end{bloc}%m
%
%
\begin{bloc}{bn}

\begin{exemple}

\QR{Déterminer le zéro de la fonction $f(x) = \dfrac{x-2}{\sqrt{x+4}-7x^3-12}$.}{$x=2$}[\vfill]
\end{exemple}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}

\Q{Déterminer le domaine des fonctions suivantes.}
\debutSousQ[itSep=0.3cm, largeurQ=5cm] 
\QR{$f(x) = -x^4+2x^3+12$}{$\dom(f) = \mathds{R}$} 
\QR{$g(x) = \dfrac{x^3-4}{x+2}$}{$\dom(g) = \mathds{R}\setminus \{-2\}$} 
\QR{$h(x) = \sqrt{x-9}$}{$\dom(h) = [9, \infty[$} 
\QR[4cm]{$i(x) = \sqrt{\dfrac{x-1}{2+x}}$}{$\dom(i) = ]{-\infty}, {-2}[ \, \cup \, [1, \infty[$} 
\QR[\linewidth]{$j(x) = \dfrac{\sqrt{x^2-4}-1}{x-7}$}{$\dom(j) = ]{-\infty}, {-2}] \, \cup \, [2, \infty[ \, \setminus \, \{7\}$} 
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{Déterminer le conjugué de $\sqrt[3]{1-x^4}-9$.}{$\sqrt[3]{1-x^4}+9$}
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?v=OV9YYl2axPg}{Addition et soustraction de fractions rationnelles.}

\href{https://www.youtube.com/watch?v=x42DyUFcwVU}{Division de fractions rationnelles.}

\href{https://www.youtube.com/watch?v=6IugSJyv_Qk&t=114s}{Division polynômiale.}

\href{https://fr.khanacademy.org/math/algebra-home/alg-functions/alg-domain-of-advanced-functions/v/domain-of-a-function?modal=1}{Domaine de fonctions algébriques. À regarder jusqu'à 4 minutes et 20 secondes.}
\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Effectuer les opérations suivantes.}
\debutSousQ
\QR{$\dfrac{3x+1}{2x^2-10x+12}-\dfrac{x^2+1}{3x^3+6x^2-24x}$}{$\dfrac{7x^3+45x^2+10x+6}{6x(x-3)(x-2)(x+4)}$}
\QR{$\dfrac{1}{x^2(x+1)^4(x-5)^3}+\dfrac{2}{x^4(x+1)^2(x-5)}$}{$\dfrac{2x^4-16x^3+13x^2+80x+50}{x^4(x-5)^3(x+1)^4}$}
\QR{$\dfrac{\frac{1}{4x}}{\frac{3}{x}+\frac{4}{x^2-x}}$}{$\dfrac{x-1}{4(3x+1)}$}
\finSousQ

\Q{Effectuer les divisions suivantes.}
\debutSousQ
\QR{$\dfrac{x^3-3x^2+4}{x+1}$}{$x^2-4x+4$}
\QR{$\dfrac{4x^4+3x^2-6}{2x+1}$}{$2x^3-x^2+2x-1-\dfrac{5}{2x+1}$}
\QR{$\dfrac{x^4+2x^3-2}{1-x^2}$}{$-x^2-2x-1+\dfrac{2x-1}{1-x^2}$} 
\finSousQ

\Q{Déterminer le domaine des fonctions suivantes.}
\debutSousQ
\QR{$f(x)=\dfrac{x^2-x+3}{x^2-x-6}$}{$\dom(f)=\mathrm{R}\setminus\{{-2}, 3\}$}
\QR{$f(x)=\dfrac{\sqrt{x^2-6x-7}}{x^2-4}$}{$\dom(f)=]{-\infty}, {-2}[ \, \cup \, ]{-2}, {-1}] \, \cup \, [7, \infty[$}
\QR{$f(x)=\sqrt[4]{2x^2-3x+3}$}{$\dom(f)=\mathrm{R}$}
\QR{$f(x)=\dfrac{4-\sqrt{9-x^2}}{x+2}$}{$\dom(f)=[{-3}, 3]\setminus\{{-2}\}$}
\QR{$f(x)=\sqrt{\dfrac{(2x+1)(x-4)}{x+4}}$}{$\dom(f)=\crochetsof*{{-4}, -\frac{1}{2}} \, \cup \, [4, \infty[$} 
\finSousQ

\QR{Donner le conjugué de $2+\sqrt{x^2-1}$.}{$2-\sqrt{x^2-1}$}

\QR{Soit la fonction $f(x) = \sqrt{x^3+1}$. Déterminer deux fonctions $g$ et $h$ telles que $f = h\circ g$.}{$g(x) = x^3+1$ et $h(x) = \sqrt{x}$\dans{em}{\columnbreak}}
\end{secExercices}
\end{bloc}%eim
