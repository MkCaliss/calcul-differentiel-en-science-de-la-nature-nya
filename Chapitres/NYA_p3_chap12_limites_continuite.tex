%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Limites et continuité}
%
%
\section{Limites}
%
%
\begin{bloc}{b}[Notation et signification]
\begin{center}
Que signifie $\dlim{x \to a}{f(x)} = b$?
\end{center}
\end{bloc}%b
%
%
\begin{bloc}{b}[Propriétés]
Nous avons vu les propriétés des limites au \infobulle{théorème \ref{thmLimiteProp}}{thmLimiteProp}.
\end{bloc}%b
%
%
\begin{bloc}{m}
Commençons par l'évaluation de limites de fonctions exponentielles et logarithmiques. On se rappelle que ces fonctions sont continues sur leur domaine.

\begin{exemple}
\Q{Évaluer les limites suivantes.}
\debutSousQ
\QS{$\dlim{x \to 2}{2^x}$}{

D'abord, le domaine de cette fonction exponentielle est $\mathds{R}$ et, comme toute fonction non définie par parties est \infobulle{continue sur son domaine}{thmContinuite}, cette fonction est continue en $x=2$. Nous pouvons donc automatiquement utiliser le fait que
$\dlim{x \to a}{f(x)} = f(a)$, ce qui découle du point 3 de la \infobulle{définition de la continuité}{defContinuiteEnUnPoint}.

Ainsi, $\dlim{x \to 2}{2^x} = 2^2 = 4$.}
\QS{$\dlim{x \to 1}{\displaystyle \frac{\log{(x)}}{2(x+5)}}$}{

De la même façon, il suffit ici d'évaluer l'image de la fonction lorsque $x=1$.

Ainsi, $\dlim{x \to 1}{\displaystyle \dfrac{\log{(x)}}{2(x+5)}}= \dfrac{\log{(1)}}{2(1+5)} = \displaystyle \frac{0}{12}=0$.
}
\finSousQ
\end{exemple}
\dans{ordi}{\newpage}

Pour l'évaluation de limites de fonctions exponentielles et logarithmiques, la connaissance de l'allure de la représentation graphique nous sera très utile. De plus, toutes les astuces vues précédemment pour les formes indéterminées peuvent s'appliquer.
\begin{exemple}
\Q{Évaluer les limites suivantes.}
\debutSousQ
\QS{$\dlim{x \to \infty}{e^x}$}{

On trace d'abord une esquisse de la représentation graphique de la fonction $e^x$. Comme $e>1$, la représentation graphique a la forme suivante.
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_12_ex_limitesExpo_1_man.pdf}
\end{center}
Comme les valeurs de $y$ ne font qu'augmenter lorsque les valeurs de $x$ augmentent, on a $\dlim{x \to \infty}{e^x}= \infty$.

Nous pouvons aussi nous faire la réflexion qu'en multipliant $e$ avec lui-même un nombre infini de fois, le résultat sera infiniment grand puisque $e>1$.
}
\dans{ordi}{\newpage}
\QS{$\dlim{x \to \infty}{\displaystyle \paren*{\frac{1}{2}}^x}$}{

Ici, $\frac{1}{2}<1$ et la représentation graphique a la forme suivante.
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_12_ex_limitesExpo_2_man.pdf}
\end{center}
Ainsi, $\dlim{x \to \infty}{\paren*{\frac{1}{2}}^x}= 0$.
}
\QS{$\dlim{x \to \infty}{\log{(x^2-x)}}$}{

\begin{minipage}[c]{0.5\textwidth}
\begin{align*}
& \ \dlim{x \to \infty}{\log{(x^2-x)}}   \hspace{1cm} & \text{F.I. } \infty - \infty \\[0.4cm]
=& \ \dlim{x \to \infty}{\log{\paren*{x^2\paren*{1-\frac{1}{x}}}}} \hspace{1cm} & \text{Forme } \log{(\infty)} \\[0.4cm]
=& \ \infty
\end{align*}
\end{minipage}  
\hspace*{1cm}
\begin{minipage}[c]{0.5\textwidth}
\includegraphics[scale=1]{Tikz/Images/chap_12_ex_limitesLog_1_man.pdf}
\end{minipage}
}
\dans{ordi}{\newpage}
\QS{$\dlim{x \to \infty}{\displaystyle \frac{e^{2x}+2e^{3x}}{e^{2x}-e^x}}$}{
\begin{align*}
& \ \dlim{x \to \infty}{\displaystyle \frac{e^{2x}+2e^{3x}}{e^{2x}-e^x}}   \hspace{2cm} & \text{F.I. } \displaystyle \frac{\infty}{\infty - \infty} \\[0.4cm]
=& \ \dlim{x \to \infty}{\displaystyle \frac{e^{3x}\paren*{\frac{1}{e^x}+2}}{e^{2x}\paren*{1-\frac{1}{e^x}}} } \\[0.4cm]
=& \ \dlim{x \to \infty}{\displaystyle \frac{e^{x}\paren*{\frac{1}{e^x}+2}}{1-\frac{1}{e^x}} } \\[0.4cm]
=& \ \infty
\end{align*}
Dans cet exemple, nous aurions pu tenter de déterminer plus précisément la nature de la forme en étudiant le comportement du dénominateur.% 
\[\dlim{x \to \infty}{\paren*{e^{2x}-e^x}} = \dlim{x \to \infty}{e^x(e^x-1)} = \infty\]
Ainsi, la forme exacte de la limite est $\frac{\infty}{\infty}$.
}
\finSousQ
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}[allowframebreaks]{bn}
\begin{exemple}

\Q{Évaluer les limites suivantes}
\debutSousQ[itSep=0.3cm]
\QR{$\dlim{x \to 4}{\displaystyle \frac{3^{\sqrt{x}}}{e^{x-4}+3}}$}{$\displaystyle \frac{9}{4}$}[\vfill]
\QR{$\dlim{x \to \infty}{\log{(x^2-5)}}$}{$\infty$}[\vfill]
\QR{$\dlim{x \to \infty}{\ln{\paren*{\displaystyle \frac{1}{x}}}}$}{${-\infty}$}[\vfill \newpage]
\framebreak
\QR{$\dlim{x \to 0}{\ln{\paren*{\dfrac{1}{x^2}}}}$}{$\infty$}[\vfill]
\QR{$\dlim{x \to 0}{\dfrac{1-e^{2x}}{1-e^{x}}}$}{$2$}[\vfill]
\finSousQ
\end{exemple}
\end{bloc}%bn 
%
%
\begin{bloc}{m}
Pour les fonctions trigonométriques, le plus avantageux sera de transformer les fonctions pour utiliser seulement les fonctions sinus et cosinus, puisque celles-ci sont continues sur les réels. Pour ce faire, on utilisera les \infobulle{définitions}{defFctTrigo}. 

\begin{exemple}
\Q{Évaluer les limites suivantes.}
\debutSousQ
\QS{$\dlim{x \to \pi}{\sec{(x)}}$}{

$\dlim{x \to \pi}{\sec{(x)}} = \dlim{x \to \pi}{\dfrac{1}{\cos{(x)}}} = \dfrac{1}{\cos{(\pi)}}=\dfrac{1}{-1}={-1}$.
}
\dans{ordi}{\newpage}
\QS{$\dlim{x \to \frac{\pi}{2}^+}{\paren[\big]{\tan{(x)}-2\sec{(x)}}}$}{
\setlength{\belowdisplayskip}{0pt}%
\begin{align*}
& \ \dlim{x \to \frac{\pi}{2}^+}{\paren[\big]{\tan{(x)}-2\sec{(x)}}}   \hspace{2cm}  \\[0.4cm]
=& \ \dlim{x \to \frac{\pi}{2}^+}{\paren*{\frac{\sin(x)}{\cos(x)}-\frac{2}{\cos{(x)}}}}   \hspace{2cm} & \text{F.I. } \infty - \infty \\[0.4cm]
=& \ \dlim{x \to \frac{\pi}{2}^+}{\dfrac{\sin(x)-2}{\cos(x)}} \hspace{2cm} & \text{F.D.} \dfrac{-1}{0^-} \\[0.4cm] %Il faudrait réussir à centrer les termes dans la fraction
=& \ \infty
\end{align*}
}
\QS{$\dlim{x \to 0^+}{\ln{\paren*{\sin(x)}}}$}{

\begin{minipage}[c]{0.5\textwidth}
\begin{align*}
& \ \dlim{x \to 0^+}{\ln{\paren*{\sin(x)}}} \hspace{1cm} & \text{Forme } \ln{\paren*{\sin(0^+)}=\ln(0^+)} \\[0.4cm]
=& \ {-\infty}  
\end{align*}
\end{minipage}  
\hspace*{0.8cm}
\begin{minipage}[c]{0.5\textwidth}
\includegraphics[scale=1]{Tikz/Images/chap_12_ex_limitesLog_2_man.pdf}
\end{minipage}

Ici, pour évaluer $\dlim{x \to 0^+}{\sin(x)}$, il faut visualiser un angle juste un peu plus grand que $0$ radian dans la représentation graphique de la fonction sinus. On constate que ce sinus sera positif et qu'il va tendre vers $0$.
}
\finSousQ
\end{exemple}

Notons que $\dlim{x \to \pm\infty}{\sin(x)}$ et $\dlim{x \to \pm\infty}{\cos(x)}$ n'existent pas puisque les fonctions sinus et cosinus sont périodiques (elles oscillent à l'infini).
\dans{ordi}{\newpage}

\begin{exemple}
\QS{Évaluer $\dlim{x \to \infty}{\cotan(x)}$.}{

$\dlim{x \to \infty}{\cotan{x}} ~~ \nexists$

Nous pouvons nous en convaincre en étudiant la représentation graphique de la fonction cotangente, qui est périodique.
\begin{center}
\begin{minipage}[c]{0.5\textwidth}
\includegraphics[scale=1]{Tikz/Images/chap_12_ex_limitesCotan_1_man.pdf}
\end{minipage}
\end{center}
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}[allowframebreaks]{bn}
\begin{exemple}

\Q{Évaluer les limites suivantes.}
\debutSousQ[itSep=0.3cm]
\QR{$\dlim{x \to \frac{\pi}{2}}{\sec(x)}$}{$\nexists$}[\vfill \newpage]
\QR{$\dlim{x \to 2\pi}{\log{\paren*{\cos(x)}}}$}{$0$}[\vspace*{3cm}]
\QR{$\dlim{x \to \frac{\pi}{4}}{\dfrac{\cos(x)-\sin(x)}{1-\tan(x)}}$}{$\dfrac{\sqrt{2}}{2}$}[\vspace*{10cm}]
\framebreak
\QR{$\dlim{x \to \infty}{\cosec(x)}$}{$\nexists$}[\vspace*{2cm}]
\QR{$\dlim{x\to \frac{\pi}{2}}{\dfrac{\sin(2x)}{\cos(x)}}$}{$2$}[\vspace*{3cm}]
\finSousQ
\end{exemple}
\end{bloc}%bn 
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Montrer que $\dlim{x \to \infty}{\dfrac{\sin{(x)}}{x}}=0$ en utilisant le \infobulle{théorème du sandwich}{thmSandwich}.}{

La valeur du sinus d'un angle est toujours comprise entre ${-1}$ et $1$. On peut donc utiliser  l'inéquation suivante.%
\[{-1}\leq \sin(x)\leq 1\]%
En divisant chaque terme par $x$, où $x>0$ puisque $x$ tend vers l'infini, on obtient%
\[-\dfrac{1}{x}\leq \dfrac{\sin(x)}{x}\leq \dfrac{1}{x}\text{ .}\]%
Or, $\dlim{x \to \infty}{-\dfrac{1}{x}}=0$ et $\dlim{x \to \infty}{\dfrac{1}{x}}=0$. Ainsi, selon le théorème du sandwich, $\dlim{x \to \infty}{\dfrac{\sin{(x)}}{x}}=0$.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}

\begin{exemple}

\QR{Évaluer la limite suivante à l'aide du théorème du sandwich.%
\[\dlim{x \to \infty}{\paren*{\frac{\sin(x)}{e^x}+3}}\]}{$3$}[\vfill]
\end{exemple}
\end{bloc}%bn 
%
%
\begin{bloc}{m}
Le théorème suivant nous sera utile pour trouver les règles de dérivation pour les fonctions trigonométriques.
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{thm}[Limite de $\displaystyle \frac{\sin(x)}{x}$ quand $x$ tend vers $0$]{thmLimiteSinxSurx}
Si $x$ est un angle mesuré en radians, alors\index{Théorème!limite $\frac{\sin(x)}{x}$}%
\[\dlim{x \to 0}{\displaystyle \frac{\sin{(x)}}{x}}=1\text{ .}\]

\preuve{

Il s'agit d'une forme indéterminée $\frac{0}{0}$, mais les astuces vues jusqu'à présent ne permettent pas de lever l'indétermination. Nous utiliserons le théorème du sandwich. Pour construire notre sandwich, observons la figure suivante. Il s'agit d'un triangle rectangle dont la base mesure $1$. L'arc en pointillé est un arc de cercle de rayon $1$ et le segment droit en pointillé relie les points d'intersection de l'arc de cercle avec le triangle rectangle.
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_12_demo_limitesSinX_surX_man.pdf}
\end{center}
Dans la figure, nous remarquons deux triangles et un secteur de cercle.
\begin{center}
\hspace*{0.65cm}
\begin{minipage}[t]{0.33\textwidth}
\includegraphics[scale=1]{Tikz/Images/chap_12_demo_limitesSinX_surX_2_man.pdf}
\end{minipage}
\hspace*{-0.4cm}
\begin{minipage}[t]{0.33\textwidth}
\includegraphics[scale=1]{Tikz/Images/chap_12_demo_limitesSinX_surX_3_man.pdf}
\end{minipage}
\hspace*{-0.7cm}
\begin{minipage}[t]{0.33\textwidth}
\includegraphics[scale=1]{Tikz/Images/chap_12_demo_limitesSinX_surX_4_man.pdf}
\end{minipage}
\end{center}

\begin{center}
{\color{green}Aire triangle vert} $\leq$ {\color{blue}Aire secteur bleu} $\leq$ {\color{red} Aire triangle rouge}
\end{center}
Calculons chacune des aires.
\dans{ordi}{\newpage}

Pour calculer l'aire des triangles, on multiplie la longueur de leur base par leur hauteur et on divise par 2.

{\color{green}Aire triangle vert} $=\displaystyle \frac{\sin(x)}{2}$. 

Pour le triangle rouge, la base mesure 1 et la hauteur représente la tangente de l'angle $x$.

{\color{red} Aire triangle rouge} $=\displaystyle \frac{1 \cdot \tan(x)}{2}=\frac{\sin(x)}{2\cos(x)}$.

Finalement, pour l'aire du secteur bleu, on utilise le produit croisé. C'est à cette étape qu'on exige que l'angle soit calculé en radians.%
\[\dfrac{\text{Aire cercle}}{2\pi}=\dfrac{\text{Aire du secteur}}{\text{Angle}}\Rightarrow\dfrac{\pi\cdot 1^2}{2\pi}=\dfrac{\text{Aire du secteur}}{x}\]%
{\color{blue}Aire secteur bleu} $=\displaystyle \frac{\pi \cdot x}{2\pi}=\frac{x}{2}$.

On obtient donc l'inéquation%
\[\dfrac{\sin(x)}{2} \leq \dfrac{x}{2} \leq \dfrac{\sin(x)}{2\cos(x)} ~\Rightarrow ~ \sin(x) \leq x \leq \dfrac{\sin(x)}{\cos(x)}.\]%
Comme nous cherchons à évaluer la limite lorsque $x$ tend vers 0, nous allons étudier deux cas deux cas.

\textbf{Cas 1:} Si $x$ tend vers 0 par la gauche, alors $\sin(x)$ tend vers 0 par la gauche, c'est-à-dire que $\sin(x)<0$. Ainsi, si on divise chaque membre de l'inéquation par $\sin(x)$, on obtient:%
\[\displaystyle 1 \geq \frac{x}{\sin(x)} \geq \frac{1}{\cos(x)}.\]% 
Puis, en inversant chaque terme, on obtient :%
\[1\leq \frac{\sin(x)}{x}\leq \cos(x) \text{ .}\]%
La dernière égalité se justifie de la même façon que $2<3 ~\Rightarrow \dfrac{1}{2}>\dfrac{1}{3}$. 

C'est le sandwich que nous recherchions. En effet, $\dlim{x \to 0^-}{1}=1$ et $\dlim{x \to 0^-}{\dfrac{1}{\cos(x)}}=1$ et donc, selon le théorème du sandwich, $\dlim{x \to 0^-}{\dfrac{\sin(x)}{x}}=1$.

\textbf{Cas 2:} Si $x$ tend vers 0 par la droite, alors $\sin(x)$ tend vers 0 par la droite, c'est-à-dire que $\sin(x)>0$. Ainsi, si on divise chaque membre de l'inéquation par $\sin(x)$, on obtient:%
\[1 \leq \dfrac{x}{\sin(x)} \leq \dfrac{1}{\cos(x)} \Rightarrow 1\geq \dfrac{\sin(x)}{x}\geq \cos(x).\]%
De la même façon, comme $\dlim{x \to 0^+}{1}=1$ et $\dlim{x \to 0^+}{\dfrac{1}{\cos(x)}}=1$, on obtient, selon le théorème du sandwich, $\dlim{x \to 0^+}{\displaystyle \frac{\sin(x)}{x}}=1$.

On peut donc conclure que $\dlim{x \to 0}{\dfrac{\sin(x)}{x}}=1$.
\finDemo 
}
\end{thm}
\dans{n}{\newpage}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}[Preuve]
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_12_demo_limitesSinX_surX_man.pdf}
\end{center}
\end{bloc}%bn
%
%
\begin{bloc}{bn}[\dans{b}{Preuve}]

\dans{b}{\hspace*{-0.3cm}}
\begin{minipage}[t]{0.33\textwidth}
\includegraphics[scale=1]{Tikz/Images/chap_12_demo_limitesSinX_surX_2_man.pdf}
\end{minipage}
\dans{n}{\hspace*{-0.3cm}}
\begin{minipage}[t]{0.33\textwidth}
\includegraphics[scale=1]{Tikz/Images/chap_12_demo_limitesSinX_surX_3_man.pdf}
\end{minipage}
\dans{n}{\hspace*{-0.3cm}}
\begin{minipage}[t]{0.33\textwidth}
\includegraphics[scale=1]{Tikz/Images/chap_12_demo_limitesSinX_surX_4_man.pdf}
\end{minipage}
\dans{n}{\newpage}
\end{bloc}%bn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Évaluer $\dlim{x \to 0}{\frac{\tan(x)}{x}}$.}{
\setlength{\belowdisplayskip}{0pt}%
\begin{align*}
& \ \dlim{x \to 0}{\frac{\tan(x)}{x}} \\[0.4cm]
=& \ \dlim{x \to 0}{\frac{\sin(x)}{x \cdot \cos(x)}} \hspace{2cm} & \text{F.I. } \frac{0}{0} \\[0.4cm]
=& \ \dlim{x \to 0}{\paren*{\frac{\sin(x)}{x} \cdot \frac{1}{\cos(x)}}}\\[0.4cm]
=& \ \dlim{x \to 0}{\frac{\sin(x)}{x}}\cdot \dlim{x \to 0} {\frac{1}{\cos(x)}}\\[0.4cm]
=& \ 1 \cdot 1 \hspace{2cm} & \text{\infobulle{théorème \ref{thmLimiteSinxSurx}}{thmLimiteSinxSurx}}\\[0.4cm]
=& \ 1 
\end{align*}
}
\end{exemple}
\dans{ordi}{\newpage}
\end{bloc}%m
%
%
\begin{bloc}{bimn}
\begin{thm}[Limite de $\dfrac{1-\cos(x)}{x}$ quand $x$ tend vers $0$]{thmLimiteUnMoinsCosxSurx}
Si $x$ est un angle mesuré en radian, alors%
\[\dlim{x\to 0}{\dfrac{1-\cos(x)}{x}}=0\text{ .}\]

\preuve{ 
\begin{alignat*}{3}
\dlim{x\to 0}{\dfrac{1-\cos(x)}{x}} &= \dlim{x\to 0}{\dfrac{\paren*{1-\cos(x)}\paren*{1+\cos(x)}}{x\paren*{1+\cos(x)}}} \hspace{2cm} && \text{multiplication par le conjugué} \\[0.3cm]
 &= \dlim{x\to 0}{\dfrac{1-\cos^2(x)}{x\paren*{1+\cos(x)}}} \hspace{2cm} && \\[0.3cm]
 &= \dlim{x\to 0}{\dfrac{\sin^2(x)}{x\paren*{1+\cos(x)}}} \hspace{2cm} && \text{\infobulle{identité trigonométrique}{propIdentitesTrigoBase}}\\[0.3cm]
 &= \dlim{x\to 0}{\dfrac{\sin(x)}{x}} \cdot \dlim{x \to 0}{\dfrac{\sin(x)}{1+\cos(x)}} \hspace{2cm} && \text{\infobulle{propriété des limites}{thmLimiteProp}}\\[0.3cm]
 &= 1 \cdot 0 && \text{\infobulle{théorème \ref{thmLimiteSinxSurx}}{thmLimiteSinxSurx}} \\[0.3cm]
 &= 0
\end{alignat*}
\finDemo
}
\end{thm}
\dans{n}{\vfill}
\end{bloc}%bimn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Évaluer la limite suivante.%
\[\dlim{x \to 0}{2x\cotan(x)}\]}{$2$}[\vfill]
\end{exemple}
\end{bloc}%bn 
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?v=xhhupTKsX1A}{Limites avec des fonctions exponentielles.}  

\href{https://www.youtube.com/watch?v=MJf0BCB_tHg}{Limites avec des fonctions logarithmiques.} 

\href{https://www.youtube.com/watch?time_continue=272&v=DCpaN5c-DbY}{Limites avec des fonctions trigonométriques.} 

\href{https://www.youtube.com/watch?time_continue=17&v=7dnoW-mWlAo}{Démonstration de $\dlim{x\to 0}{\dfrac{\sin(x)}{x}}=1$.}
\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Évaluer les limites.}
\debutSousQ
\QR{$\dlim{x \to \infty}{e^{\frac{1}{x}}$}}{$1$}
\QR{$\dlim{x \to 0^-}{e^{\frac{1}{x}}$}}{$0$}
\QR{$\dlim{x \to \infty}{\dfrac{1-e^x}{1+e^x}$}}{${-1}$}
\QR{$\dlim{x \to 2^-}{\ln(2-x)$}}{${-\infty}$ donc $\nexists$}
\QR{$\dlim{x \to 0^+}{e^{\frac{1}{x}}$}}{$\infty$ donc $\nexists$}
\QR{$\dlim{x \to 2}{\dfrac{x3^x}{4^x-x}$}}{$\dfrac{9}{7}$}
\QR{$\dlim{x \to \infty}{\paren*{\dfrac{1}{2}}^{\sqrt{x}}$}}{$0$}
\QR{$\dlim{x \to 0}{\log_6(x^2+1)$}}{$0$}
\QR{$\dlim{x \to 4}{\log_3(7x-1)$}}{$3$}
\QR{$\dlim{x \to 0}{\log_2\paren*{\dfrac{1}{x^2}}$}}{$\infty$ donc $\nexists$}
\QR{$\dlim{x \to 1}{\paren*{(\ln(x))^3-2}^5$}}{${-32}$}
\QR{$\dlim{x \to 4}{\dfrac{e^{x^2}}{\sqrt{x}-2}$}}{$\nexists$}
\QR{$\dlim{x \to 0}{\sqrt{3^x-1}$}}{$0$}
\finSousQ

\Q{Évaluer les limites.}
\debutSousQ
\QR{$\dlim{x \to \frac{\pi}{4}}{\dfrac{x \sin(x)}{\pi}$}}{$\dfrac{\sqrt{2}}{8}$}
\QR{$\dlim{x \to \frac{\pi}{3}}{\cotan(x)$}}{$\dfrac{\sqrt{3}}{3}$}
\QR{$\dlim{x \to 0^-}{e^{\cosec(x)}}$}{$0$}
\QR{$\dlim{x \to \frac{\pi}{2}}{\dfrac{\cos(x)+1}{3\sin(x)}}$}{$\dfrac{1}{3}$}
\QR{$\dlim{x \to 1}{\dfrac{4^{2x+1}+\cotan\paren*{\dfrac{\pi x}{2}}}{2^{3x}+\ln(x)}}$}{$8$}
\QR{$\dlim{x \to 0}{\dfrac{x}{\sin(x)}}$}{$1$}
\QR{$\dlim{x \to 0}{\dfrac{2\sin(x)}{x^2+x}}$}{$2$}
\QR{$\dlim{x \to 0}{\dfrac{\cos(x)}{x}}$}{$\nexists$}
\QR{$\dlim{x \to \frac{\pi}{4}}{\dfrac{\cos(x)-\sin(x)}{1-\tan(x)}}$}{$\dfrac{\sqrt{2}}{2}$}
\QR{$\dlim{x \to 1}{\cos\paren*{\dfrac{x^2-1}{x-1}}}$}{$\cos(2)$}
\QR{$\dlim{x \to 0}{\sin\paren*{\dfrac{1}{x}}}$}{$\nexists$}

\QR{$\dlim{x \to 0}{\sin\paren*{\dfrac{x^2-\pi x}{x}}}$}{$0$}
\QR{$\dlim{x \to 0}{\dfrac{\sin(3x)}{\tan(3x)}}$}{$1$}
\QR{$\dlim{x \to \frac{\pi}{2}^+}{e^{\tan(x)}}$}{$0$}
\QR{$\dlim{x \to 0^+}{\paren[\Big]{\ln\paren[\big]{\sin(2x)}-\ln\paren[\big]{\tan(x)}}}$}{$\ln(2)$}
\QR{$\dlim{x \to \infty}{\arctan(x)}$}{$\dfrac{\pi}{2}$}
\finSousQ

\Q{Utiliser le théorème du sandwich pour évaluer les limites suivantes.}
\debutSousQ
\QR{$\dlim{x \to \infty}{\dfrac{\cos(2x)}{x^2}}$}{$0$}
\QR{$\dlim{x \to 0^+}{\sqrt{x}e^{\sin\paren*{\dfrac{\pi}{x}}}}$}{$0$}
\QR{$\dlim{x \to \infty}{\dfrac{\sin(x)+2}{3x+2}}$}{$0$}
\QR{$\dlim{x \to 0}{x\sin\paren*{\dfrac{1}{x}}}$}{$0$}
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Continuité}
%
%
\begin{bloc}{m}
Les fonctions qui ne sont pas définies par parties sont continues sur leur domaine. Pour étudier la continuité aux valeurs charnières des fonctions définies par parties, les limites étudiées dans la section précédente seront utiles. 

\begin{exemple}
\QS{Déterminer l'ensemble sur lequel la fonction suivante est continue.%
\[f(x) =
\begin{cases}
2^x & \text{si } x\leq 0 \\[0.2cm]
\cosec(x) & \text{si } 0<x<2\pi \\[0.2cm]
\displaystyle \frac{\ln(x)}{\sqrt{10-x}} & \text{si } x>2\pi 
\end{cases}\]}{

Il faut d'abord étudier la continuité sur chaque branche. Le domaine de la fonction $y=2^x$ est $\mathds{R}$, donc la fonction $f$ est continue sur $]{-\infty}, 0[$.

Comme $\cosec(x)=\dfrac{1}{\sin(x)}$, le domaine de la fonction $y=\cosec(x)$ est $\mathds{R} \setminus \{x \vert \sin(x)=0\}=\mathds{R} \setminus \{k\pi ~\vert~ k \in \mathds{Z}\}$, donc la fonction $f$ est continue sur $]0, 2\pi[ \setminus \{\pi\}$.

Le domaine de la fonction $\dfrac{\ln(x)}{\sqrt{10-x}}$ est $]0, 10[$, donc la fonction $f$ est continue sur $]2\pi, 10[$.

On étudie ensuite la continuité à la valeur charnière $x=0$.

\begin{liste}
\item $f(0) = 2^0=1$ donc $0 \in \dom(f)$. 

\item
\begin{minipage}[t]{0.35\textwidth}
\begin{align*}
\dlim{x \to 0^-}{f(x)} =& \ \dlim{x \to 0^-}{2^x}\\[0.4cm]
 =& \ 1 
\end{align*} 
\end{minipage}
\begin{minipage}[t]{0.65\textwidth}
\begin{align*}
\dlim{x \to 0^+}{f(x)} =& \ \dlim{x \to 0^+}{\cosec(x)} \\[0.3cm]
=& \ \dlim{x \to 0^+}{\frac{1}{\sin(x)}}  \hspace{1cm} & \text{Forme }\frac{1}{0^+} \\[0.3cm]
=& \ \infty 
\end{align*}
\end{minipage}
\end{liste}
Comme la limite n'existe pas, la fonction $f$ n'est pas continue en $x=0$.

De plus, comme $f(2\pi) ~\nexists$, la fonction $f$ est  discontinue en $x=2\pi$.

En conclusion, la fonction $f$ est continue $\forall ~ x~ \in ]{-\infty}, 10[   \ \setminus \ \{0, \, \pi, \, 2\pi\}$.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\onslide<+-> \alert<1>{Nous avons vu au \infobulle{théorème \ref{thmContinuite}}{thmContinuite} que toutes les fonctions usuelles non définies par parties sont continues sur leur \infobulle{domaine}{tabCalculsImpossiblesFinal}.}

\onslide<+-> \alert<2>{De plus, la \infobulle{définition de la continuité en un point}{defContinuiteEnUnPoint} est toujours la même.}

\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exemple}

\QR{Déterminer l'ensemble sur lequel la fonction suivante est continue.
\[f(x) =
\begin{cases}
2^x\sqrt{x+16} & \text{si } x\leq 0 \\
\tan(x) & \text{si } 0<x\leq 2\pi \\
\ln(x^2-9) & \text{si } x > 2\pi\\
\end{cases}\]}{$[{-16}, \infty[ \ \setminus \ \acco*{0, \frac{\pi}{2}, \frac{3\pi}{2}, 2\pi}$}[\vfill]
\end{exemple}
\end{bloc}%bn 
%
%
\devoirs
%
%
\dans{n}{\newpage}
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Déterminer pour quelles valeurs de $x$ la fonction $f$ est continue.}
\debutSousQ
\QR{
$f(x) =
\begin{cases}
\cos(x) & \text{si } x \leq \frac{\pi}{2} \\
\sin(2x) & \text{si } \frac{\pi}{2} < x < 2\pi \\
\sqrt{x} & \text{si }  x > 2\pi 
\end{cases}$}{$\mathds{R}\setminus\{2\pi\}$}
\QR{$f(x)=\sin\paren*{\dfrac{x+3}{x^2-1}}$}{$\mathds{R}\setminus\{{-1}, 1\}$} 
\QR{$f(x)=\dfrac{\sqrt{x}+\cos(x)}{\sin(x)-1}$}{$[0, \infty[ \ \setminus \  \acco*{\dfrac{\pi}{2}+2k\pi|k \, \in \, \mathds{Z}}$} 
\QR{$f(x)=\ln\paren*{\dfrac{3+x}{x-2}}$}{$]{-\infty}, {-3}[ \, \cup \, ]2, \infty[$}
\QR{$f(x)=\dfrac{\arctan(x)+\ln(x)}{x^2-4}$}{$]0, 2[ \, \cup \, ]2, \infty[$}
\QR{$f(x)=e^{\frac{\sin(x)}{x}}$}{$\mathds{R}\setminus\{0\}$}
\QR{$f(x)=\dfrac{\arcsin\paren*{\dfrac{1}{x}}}{x}$}{$\mathds{R} \ \setminus \ ]{-1}, 1[$}  
\finSousQ
\end{secExercices}
\end{bloc}%eim

