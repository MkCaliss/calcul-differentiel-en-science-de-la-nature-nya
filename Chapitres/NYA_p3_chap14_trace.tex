%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Représentation graphique des fonctions transcendantes}
%
%
\section{Croissance, décroissance et extremums relatifs}
%
%
\begin{bloc}{m}
Nous avons vu que pour déterminer la \infobulle{croissance et la décroissance}{defCroissanceDecroissance} d'une fonction, il faut étudier le signe de la dérivée première de la fonction. Le tout est résumé au \infobulle{théorème \ref{thmCroissanceDecroissance}}{thmCroissanceDecroissance}. Puisque ce théorème ne dépend pas du type de fonction, il s'applique aussi pour les fonctions transcendantes. 

De plus, nous avons vu que les valeurs de $x$ où la fonction peut potentiellement passer de croissante à décroissante, ou vice versa, sont les \infobulle{valeurs critiques}{defValeurCritique} de la fonction. À ces valeurs critiques, on peut, ou non, retrouver des \infobulle{extremums relatifs}{defExtremumsRelatifs}. Pour le vérifier, nous pouvons utiliser le \infobulle{test de la dérivée première}{thmCritereDeriveePremiere} ou le \infobulle{test de la dérivée seconde}{thmCritereDeriveeSeconde}. 

\begin{exemple}
\QS{Déterminer les intervalles de croissance et de décroissance ainsi que les extremums relatifs de la fonction $f(x) =\dfrac{2}{e^{x^2}}+3$.}{

Nous avons $\dom(f)=\mathds{R}$ puisque $e^{x^2}$ n'égale jamais zéro. 

Pour trouver les valeurs critiques de $f$, nous trouvons sa dérivée. 

Pour ce faire, on peut d'abord réécrire la fonction de la façon suivante: $f(x) = 2e^{-x^2}+3$.

Nous obtenons $f'(x) = -\dfrac{4x}{e^{x^2}}$. 

Comme $\dom(f')=\mathds{R}$, il n'y a aucune valeur de $x$ telle que $f'(x) ~\nexists$. 

De plus, $f'(x)=0 \Rightarrow 4x=0 \Rightarrow x=0$. Comme cette valeur fait partie du domaine de $f$, il s'agit d'une valeur critique, elle est donc potentiellement l'abscisse d'un extremum relatif. 

\newpage

Nous construisons ensuite le tableau de signes en tenant compte du domaine et de la valeur critique trouvée.
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{2cm}|C{1.5cm}|C{2cm}|}
\hline
$x$ & & $0$ &  \\ \hline
$f'(x)$ & $+$ & $0$ & $-$ \\ \hline
$f(x)$ & $\nearrow$ & max. rel. $5$ & $ \searrow$  \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
Donc, $f$ est croissante sur $]{-\infty}, 0]$ et décroissante sur $[0, \infty[$. De plus, $f$ possède un maximum relatif de 5, atteint lorsque $x=0$. 
}
\end{exemple}

\begin{exemple}
\QS{Trouver les extremums relatifs de la fonction $f(\theta)=\cos^2{(2\theta)}$ sur l'intervalle $]0,\frac{\pi}{2}[$ en utilisant le \infobulle{test de la dérivée seconde}{thmCritereDeriveeSeconde}.}{

Nous avons $\dom(f) = \mathds{R}$. Ici, nous nous limiterons à l'intervalle $\crochetso*{0,\frac{\pi}{2}}$.

Nous obtenons $f'(\theta) = -4\cos(2\theta)\sin(2\theta)$. 

Nous pouvons simplifier cette expression en utilisant une \infobulle{identité trigonométrique}{propAutresIdentitesTrigo}.

Ainsi, $f'(\theta)={-2}\sin(4\theta)$.

Ensuite, en dérivant $f'$, nous obtenons $f''(\theta) ={-8}\cos(4\theta)$. 

Nous avons $f'(\theta)=0$ si $\sin(4\theta)=0\Rightarrow 4\theta = 0$, $\pi$ ou $2\pi \Rightarrow \theta =0$, $\dfrac{\pi}{4}$ ou $\dfrac{\pi}{2}$. Comme $0$ et $\dfrac{\pi}{2}$ ne font pas partie de l'intervalle étudié, $\theta=\dfrac{\pi}{4}$ est la seule valeur critique où $f'(\theta)=0$.

De plus, $\dom(f')=\mathds{R}$, donc il n'y a aucune valeur $\theta$ dans l'intervalle telle que $f'(\theta)~\nexists$.

Ainsi, sur cet intervalle, le seul candidat est $\theta=\dfrac{\pi}{4}$.

Comme $f''\paren*{\dfrac{\pi}{4}}=8 > 0$, la fonction $f$ admet un minimum relatif en $x=\dfrac{\pi}{4}$, selon le \infobulle{test de la dérivée seconde}{thmCritereDeriveeSeconde}. Ce minimum est $f\paren*{\dfrac{\pi}{4}}=0$.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{b}[Croissance et décroissance]

Pour trouver les intervalles de \infobulle{croissance et de décroissance}{defCroissanceDecroissance} d'une fonction $f$, nous utilisons le \infobulle{théorème \ref{thmCroissanceDecroissance}}{thmCroissanceDecroissance}.
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item Trouver le domaine de $f$. 

\item Trouver les \infobulle{valeurs critiques}{defValeurCritique} de $f$. 

Attention au domaine de $f$ et $f'$. 

\item Faire un tableau de signes de la dérivée première. 
\end{liste}
\end{bloc}%b
%
%
\begin{bloc}{bn}
\begin{exercice}

\Q{Déterminer les intervalles de croissance et de décroissance, ainsi que les extremums relatifs, des fonctions suivantes.}
\debutSousQ[itSep=0.3cm]
\QR{$f(x) = \ln\paren*{\sqrt{x^2+4}}$}{La fonction $f$ est croissante sur $[0, \infty[$ et décroissante sur $]{-\infty}, 0]$. Elle possède un minimum relatif de $\ln(2)$ atteint en $x=0$.}
\QR{$f(x) = -\arctan(x^2-1)$}{La fonction $f$ est croissante sur $]{-\infty}, 0]$ et décroissante sur $[0, \infty[$. Elle possède un maximum relatif de $\frac{\pi}{4}$ atteint en $x=0$.} 
\finSousQ
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?v=O82FRyA5fzY&list=PLQhcwa4wrwRpsC-Y-NwaFxb3ydas0bI96&index=3}{Trouver les extremums relatifs (fonction exponentielle).}

\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}

\Q{Déterminer les intervalles de croissance et de décroissance des fonctions suivantes, ainsi que les abscisses des extremums relatifs.}
\debutSousQ
\QR{$f(x)=\ln\paren*{\dfrac{x^2}{x^2-1}}$}{Croissante sur $]{-\infty}, -1[$. Décroissante sur $]1, \infty[$. Aucun extremum relatif.}[12cm]  
\QR{$f(x)=\arcsin(x^2)$}{Croissante sur $[0, 1]$. Décroissante sur $[-1, 0]$. Minimum relatif en $x=0$. Maximum relatif en $x=-1$ et en $x=1$.}[12cm]  
\QR{$f(x)=\sin(x^2)$ sur $\crochets*{-\sqrt{\pi}, \sqrt{\pi}\thinspace}$}{Croissante sur $\crochets*{-\sqrt{\pi}, -\sqrt{\frac{\pi}{2}}}$ et sur $\crochets*{0, \sqrt{\frac{\pi}{2}}}$. Décroissante sur $\crochets*{-\sqrt{\frac{\pi}{2}}, 0}$ et sur $\crochets*{\sqrt{\frac{\pi}{2}}, \sqrt{\pi}}$. Minimum relatif en $x=0$, $x=-\sqrt{\pi}$ et en $x=\sqrt{\pi}$. Maximum relatif en $x=-\sqrt{\frac{\pi}{2}}$ et en $x=\sqrt{\frac{\pi}{2}}$.}[12cm]  
\QR{$f(x)=e^x+\dfrac{e^x}{x}$}{Croissante sur $\crochets*{{-\infty}, \frac{-1-\sqrt{5}}{2}}$ et sur $\crochetsfo*{\frac{\sqrt{5}-1}{2},\ \infty}$. Décroissante sur $\crochets*{\frac{-1-\sqrt{5}}{2}, \frac{\sqrt{5}-1}{2}}\setminus\{0\}$. Minimum relatif en $x=\frac{\sqrt{5}-1}{2}$. Maximum relatif en $x=\frac{-1-\sqrt{5}}{2}$.} 
\finSousQ

\end{secExercices}
\end{bloc}%eim
%
%
\section{Concavité et points d'inflexion}   \label{secConcaviteTrans}
%
%
\begin{bloc}{m}
Pour déterminer la concavité d'une fonction $f$, il faut étudier le signe de sa dérivée seconde. Le tout est résumé au \infobulle{théorème \ref{thmConcavite}}{thmConcavite}. Puisque ce théorème ne dépend pas du type de fonction, il s'applique aussi pour les fonctions transcendantes. 

Les \infobulle{points d'inflexion}{defPointInflexion} sont les endroits où la fonction change de concavité et ils se trouvent potentiellement aux endroits où la dérivée seconde s'annule ou n'existe pas. 

Nous utiliserons donc le tableau de signes de la dérivée seconde pour déterminer les intervalles de concavité vers le haut et vers le bas et identifier les valeurs où on retrouve des points d'inflexion.
\end{bloc}%m
%
%
\begin{bloc}{b}[Concavité et points d'inflexion]
Pour déterminer les intervalles de concavité vers le haut et vers le bas d'une fonction $f$ (\infobulle{théorème \ref{thmConcavite}}{thmConcavite}), ainsi que pour trouver les \infobulle{points d'inflexion}{defPointInflexion} de $f$ : 
\begin{liste}[itSep=0.3cm, beaOpt=<+- |alert@+>]
\item Trouver le domaine de $f$. 
\item Trouver les endroits où la dérivée seconde s'annule ou n'existe pas. 

Attention au domaine de $f$ et $f''$. 
\item Faire un tableau de signes de la dérivée seconde. 
\end{liste}
\end{bloc}%b
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Déterminer les intervalles de concavité vers le haut et de concavité vers le bas, ainsi que les points d'inflexion de la fonction $f(x)=\ln(x^2+1)$.}{

Nous avons $\dom(f) = \mathds{R}$ puisque $x^2+1$ est toujours positif. 

Nous trouvons les deux premières dérivées. 

Nous obtenons $f'(x) = \dfrac{2x}{x^2+1}$ et $f''(x) = \dfrac{{-2}(x-1)(x+1)}{(x^2+1)^2}$. 

Nous déterminons les valeurs de $x$ pour lesquelles la deuxième dérivée s'annule ou n'existe pas. Il s'agit des endroits où la concavité peut changer. 

Nous trouvons $\dom(f'') = \mathds{R}$, donc il n'y a aucune valeur de $x$ telle que $f''(x)~\nexists$.

De plus, $f''(x)=0 \Rightarrow x={-1}$ ou $x=1$. Comme ces deux valeurs font partie du domaine de $f$, il s'agit possiblement d'abscisses de points d'inflexion, ce que nous confirmerons avec le tableau de signes.
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{2cm}|C{2cm}|C{1.4cm}|C{2cm}|C{1.4cm}|C{2cm}|}
\hline
$x$ & &  ${-1}$  &  & 1 & \\ \hline
$f''(x)$ & $-$ & $0$ & $+$ & 0 & $-$   \\ \hline
$f(x)$ & $\cap$ & pt. inf. $\ln(2)$ &  $\cup$ &  pt. inf. $\ln(2)$ & $\cap$  \\ \hline
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}
La fonction est donc concave vers le haut sur $[{-1}, 1]$ et concave vers le bas sur $]{-\infty}, {-1}]$ et sur $[1, \infty[$. De plus, cette fonction admet deux points d'inflexion $({-1}, \ln(2))$ et $(1, \ln(2))$.
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{Déterminer les intervalles de concavité vers le haut et vers le bas, ainsi que les points d'inflexion, de la fonction $f(x) = e^x-e^{-x}$.}{La fonction $f$ est concave vers le haut sur $[0, \infty[$. Elle est concave vers le bas sur $]{-\infty}, 0]$. Elle possède un point d'inflexion au point $(0, 0)$.} 
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{m}
\subsection{Ressources additionnelles}

\href{https://www.youtube.com/watch?v=SxHQjnQFHwM&list=PLQhcwa4wrwRpsC-Y-NwaFxb3ydas0bI96&index=5}{Trouver les points d'inflexion (fonction logarithmique).}
\end{bloc}%m
%
%
\begin{bloc}{eim}
\begin{secExercices}
\Q{Déterminer les intervalles sur lesquels les fonctions suivantes sont concaves vers le haut et vers le bas, ainsi que les abscisses des points d'inflexion.}
\debutSousQ
\QR{$f(x)=xe^x$}{Concave vers le bas sur $]{-\infty}, {-2}]$. Concave vers le haut sur $[{-2}, \infty[$. Point d'inflexion en $x={-2}$.}[12cm]   
\QR{$f(x)=2x+\arcsin(1-x)$}{Concave vers le bas sur $[1, 2]$. Concave vers le haut sur $[0, 1]$. Point d'inflexion en $x=1$.}[12cm]  
\QR{$f(x)=\ln\paren*{\dfrac{e^{2x+1}}{(e^x-1)^2}}$}{Concave vers le haut sur $\mathds{R}\setminus\{0\}$. Aucun point d'inflexion.} 
\QR{$f(x)=8\sin(x)-\tan(x)$ où $x \, \in \crochetso*{\dfrac{-\pi}{2}, \dfrac{\pi}{2}}$}{Concave vers le bas sur $\crochetsfo*{0, \frac{\pi}{2}}$. Concave vers le haut sur $\crochetsof*{-\frac{\pi}{2}, 0}$. Point d'inflexion en $x=0$.\dans{e}{\newpage}}[12cm]  
\finSousQ
\end{secExercices}
\end{bloc}%eim
%
%
\section{Tracé des fonctions transcendantes}
%
%
\begin{bloc}{bmn}
Pour le tracé de fonctions transcendantes, nous utiliserons la \infobulle{démarche}{tabMethodeTraceFct} vue dans les sections précédentes.
\end{bloc}%bmn
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Faire l'étude complète de la fonction $f(x) = \sqrt{x} \ \ln(x)$ sachant que $\dlim{x \to 0^+}{\sqrt{x} \ln{(x)}}=0$.}{

\textbf{Étape 1: Trouver le domaine de la fonction.} 

Nous obtenons $\dom(f) = ]0, \infty[$.\\

\textbf{Étape 2: Déterminer si la fonction possède des asymptotes horizontales et verticales.} 

La seule valeur de $x$ possible où il peut y avoir une asymptote verticale est $x=0$, à cause du logarithme. Or, $\dlim{x \to 0^+}{\sqrt{x}\ln{(x)}}=0$, donc il n'y a pas d'asymptote verticale.

Ici, nous n'évaluons pas la limite quand $x$ tend vers 0 par la gauche en raison de la forme du domaine.

On étudie ensuite le comportement à l'infini de la fonction.

$\dlim{x \to \infty}{\sqrt{x} \ln{(x)}}=\infty$

Il n'y a donc pas d'asymptote horizontale lorsque $x$ tend vers l'infini. 

Nous n'avons pas à vérifier l'existence d'asymptote horizontale à gauche à cause du domaine.\\

\textbf{Étape 3: Trouver les points critiques de la fonction (pour trouver les lieux potentiels des extremums relatifs).}  

Nous trouvons $f'(x) = \dfrac{\ln(x) + 2}{2\sqrt{x}}$. 

Nous avons $\dom(f') = ]0, \infty[$. Il n'y a donc pas de valeur de $x$ élément du domaine de $f$ telle que $f'(x)~\nexists$.  

Ensuite, $f'(x) = 0 \Rightarrow \ln{(x)}+2 = 0 \Rightarrow \ln(x)={-2} \Rightarrow x=e^{-2}$. Comme cette valeur fait partie du domaine de $f$, il s'agit d'une valeur critique de $f$.  \dans{ordi}{\newpage}

\textbf{Étape 4: Trouver les endroits du domaine de $f$ où la dérivée seconde s'annule ou n'existe pas (pour trouver les lieux potentiels des points d'inflexion).}  

Nous trouvons $f''(x) = -\dfrac{\ln(x)}{4\sqrt{x^3}}$. 

Nous avons  $\dom(f'') = ]0, \infty[$. Il n'y a donc pas de valeur de $x$ élément du domaine de $f$ telle que $f''(x)~\nexists$. 

De plus, $f''(x) = 0 \Rightarrow -\ln(x)=0 \Rightarrow x=1$. Puisque $x=1$ fait partie du domaine de $f$, il s'agit potentiellement de l'abscisse d'un point d'inflexion. \\ 

\textbf{Étape 5: Faire un tableau de signes.}  
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{1.5cm}|C{1cm}|C{1.2cm}|C{1.4cm}|C{1.2cm}|C{1.4cm}|C{1.3cm}|}
\hline
$x$        & $0$ &   & $e^{-2}$ &   & $1$ &        \\ \hline
$f'(x)$ & $\nexists$  & $-$  & $0$  & $+$ & $+$  & $+$   \\ \hline
$f''(x)$   & $\nexists$  & $+$ & $+$  & $+$  & $0$ & $-$   \\ \hline
$f(x)$ & $\nexists$  &  \fdh  & min. rel. $-\frac{2}{e}$ & \fch    & pt. inf. $0$  & \fcb     \\ \hline  
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}


\textbf{Étape 6: Déterminer les zéros et l'ordonnée à l'origine, si nécessaire.}  

La fonction passe par le point $(1, 0)$. \\
 
\textbf{Étape 7: Tracer le graphique.} 
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_14_ex_trace_1_man.pdf}
\end{center}
}
\end{exemple}
\dans{ordi}{\newpage}



\begin{exemple}
\QS{Faire l'étude complète de la fonction $f(x) = 2x+\arccos(x)$.}{

\textbf{Étape 1: Trouver le domaine de la fonction.} 

Nous obtenons $\dom(f) = [{-1}, 1]$. \\

\textbf{Étape 2: Déterminer si la fonction possède des asymptotes horizontales et verticales.} 

Il n'y a aucun candidat possible pour les asymptotes verticales. Il n'y a donc pas d'asymptote verticale. 

De plus, à cause du domaine, il n'y a pas d'asymptote horizontale.\\

\textbf{Étape 3: Trouver les points critiques de la fonction (pour trouver les lieux potentiels des extremums relatifs). } 

Nous trouvons $f'(x) = 2-\dfrac{1}{\sqrt{1-x^2}}$. 

Nous avons $\dom(f') = ]{-1}, 1[$, donc $x={-1}$ et $x=1$ sont des valeurs critiques, car la dérivée n'existe pas pour ces valeurs.  

Ensuite, $f'(x) = 0 \Rightarrow \dfrac{1}{\sqrt{1-x^2}} = 2 \Rightarrow \sqrt{1-x^2}=\dfrac{1}{2} \Rightarrow 1-x^2=\dfrac{1}{4} \Rightarrow x^2=\dfrac{3}{4} \Rightarrow x= \pm \dfrac{\sqrt{3}}{2}$. 

Comme ces valeurs font partie du domaine de $f$, il s'agit de valeurs critiques de $f$.  \\

\textbf{Étape 4: Trouver les endroits du domaine de $f$ où la dérivée seconde s'annule ou n'existe pas (pour trouver les lieux potentiels des points d'inflexion).}  

Nous trouvons $f''(x) = -\dfrac{x}{\sqrt{(1-x^2)^3}}$. 

Nous avons  $\dom(f'') = ]-1, 1[$. Donc $f''(-1)$ et $f''(1) ~\nexists$, mais $x={-1}$ et $x=1$ ne peuvent pas être les abscisses de points d'inflexion puisqu'il s'agit des bornes du domaine. 

De plus, $f''(x) = 0 \Rightarrow x=0$. Puisque $x=0$ fait partie du domaine de $f$, il s'agit potentiellement de l'abscisse d'un point d'inflexion. 

\newpage

\textbf{Étape 5: Faire un tableau de signes.} 
\begin{center}
\renewcommand{\arraystretch}{1.6}
\begin{tabular}{|C{1cm}|C{1.5cm}|C{0.8cm}|C{1.8cm}|C{0.8cm}|C{1.3cm}|C{0.8cm}|C{1.5cm}|C{0.8cm}|C{1.4cm}|}
\hline
$x$        & ${-1}$ &   & $-\frac{\sqrt{3}}{2}$ &   & $0$ &  &   $\frac{\sqrt{3}}{2}$ &  &  $1$    \\ \hline
$f'(x)$ & $\nexists$  & $-$  & $0$  & $+$ & $+$  & $+$ & $0$ & $-$ & $\nexists$  \\ \hline
$f''(x)$   & $\nexists$  & $+$ & $+$  & $+$  & $0$ & $-$ & $-$ & $-$ & $\nexists$  \\ \hline
$f(x)$ & max. rel. $\pi-2$  &  \fdh  & min. rel. $-\sqrt{3}+\frac{5\pi}{6}$ & \fch    & pt. inf. $\frac{\pi}{2}$  & \fcb & max. rel. $\sqrt{3}+\frac{\pi}{6}$ & \fdb & min. rel. \ \ \ $2$     \\ \hline  
\end{tabular}
\renewcommand{\arraystretch}{1}
\end{center}

\textbf{Étape 6: Déterminer les zéros et l'ordonnée à l'origine, si nécessaire.}  

Pas nécessaire pour cet exemple. \newpage
 
\textbf{Étape 7: Tracer le graphique.}
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_14_ex_trace_2_man.pdf}
\end{center}
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{bn}

\begin{exercice}

\QR{Faire l'étude complète de la fonction $f(x) = \dfrac{e^x}{x}$ sachant que $\dlim{x \to \infty}{\dfrac{e^x}{x}}=\infty$.}{
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_14_exerc_trace_1_bn.pdf}
\end{center}
\newpage
} 
\end{exercice}
\end{bloc}%bn
%
%
\begin{bloc}{bn}
\begin{exercice}

\QR{Faire l'étude complète de la fonction $f(x) = \sin(x)+\cos(x)$ sur $[0, \ \pi]$.}{
\begin{center}
\includegraphics[scale=1]{Tikz/Images/chap_14_exerc_trace_2_bn.pdf}
\end{center}
} 
\end{exercice}
\end{bloc}%bn
%
%
\devoirs
%
%
\begin{bloc}{eim}
\begin{secExercices}

\Q{Faire l'étude complète des fonctions suivantes pour donner la représentation graphique.}
\debutSousQ
\QR{$f(x)=\arctan(e^x)$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_14_exerc_trace_1_man.pdf}
}

\QR{$f(x)=e^{\frac{1}{x}}$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_14_exerc_trace_2_man.pdf}
}

\QR{$f(x)=\dfrac{\cos(x)}{2+\sin(x)}$ sur $[0, 2\pi]$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_14_exerc_trace_3_man.pdf}
}

\QR{$f(x)=x + \ln(x^2)$ \newline sachant que $\dlim{x \to {-\infty}}{f(x)}={-\infty}$}{
\includegraphics[scale=1, valign=t]{Tikz/Images/chap_14_exerc_trace_4_man.pdf}
\dans{m}{\columnbreak}
}
\finSousQ
\end{secExercices}
\end{bloc}%eim

