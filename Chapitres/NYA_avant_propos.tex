%%% Copyright (C) 2020 Julie Gendron, Simon Paquette et Jean-Nicolas Pépin
%%%
%%% Information de contact : latex.multidoc@gmail.com
%%%
%%% Il est permis de partager (copier, distribuer ou communiquer) ou adapter 
%%% (remixer, transformer et crer à partir) ce logiciel sous les termes de 
%%% la licence Attribution-NonCommercial-ShareAlike 4.0 International 
%%% (CC BY-NC-SA 4.0) sous les conditions d'attribution, de ne pas en faire une
%%% utilisation commerciale et de partager dans les mêmes conditions.
%%%
%%% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
%%% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
%%% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\begin{bloc}{mi}

\chapter*{Avant-propos}
\addcontentsline{toc}{chapter}{\protect\numberline{}Avant-propos}

\section*{Organisation du document}

L'approche traditionnelle pour les cours de calcul différentiel consiste à aborder tous les concepts, l'un après l'autre, et à chaque fois, analyser leurs implications pour chacun des types de fonctions. Cette approche ne semble pas donner de résultats satisfaisants. Le premier problème est que les étudiants et les étudiantes rencontrent des difficultés, principalement d'ordre algébrique, très tôt dans le cours, menant parfois à un certain découragement. Le second problème, sous-jacent, est que le temps nécessaire à une bonne compréhension des concepts est utilisé pour tenter de surmonter les difficultés algébriques. Ainsi, on remarque, dans le restant du parcours collégial, que les concepts du cours de calcul différentiel ne sont pas maîtrisés.

Afin de faciliter la compréhension des concepts, mais aussi la transition du secondaire au collégial, nous avons décidé d'organiser le cours différemment. Nous avons choisi de diviser le cours selon les divers types de fonctions: les polynômes, les autres fonctions algébriques et, finalement, les fonctions transcendantes. Ainsi, l'apprentissage de tous les concepts du cours se fera très tôt dans la session, en travaillant uniquement avec des polynômes. L'attention sera plutôt portée sur les concepts, avec des calculs de niveau élémentaire. De plus, en abordant les applications du calcul différentiel, on pourra répondre assez rapidement à la question : \og À quoi ça sert? \fg{} plutôt que d'y répondre au terme du cours. De plus, chaque concept sera revu à trois reprises, permettant une meilleure rétention en vue de la suite de votre parcours collégial.


Ce document a été produit afin de faciliter votre apprentissage. Les exemples, les présentations en classe et les exercices ont été choisis minutieusement. Toutefois, il n'est pas impossible que des erreurs se soient glissées dans le document. Nous vous serions reconnaissants de bien vouloir nous informer de toute erreur que vous auriez remarquée. 


Bonne session!

\section*{Utilisation du document}

Ce document n'est pas conçu pour être imprimé. Cependant, les séries d'exercices ont volontairement été formatées à ce qu'elles le soient facilement. Un document contenant uniquement les séries d'exercices peut vous être fourni si vous en faites la demande à votre professeure ou professeur.

\dans{ordi}{Pour une pleine appréciation des fonctionnalités du document, il est préférable d'utiliser le lecteur PDF \emph{Acrobat Reader}. Quoiqu'il ne s'agisse pas nécessairement du meilleur outil de la sorte, il offre des fonctionnalités que les autres n'offrent pas. De plus, il est gratuit. Malheureusement, à notre connaissance, aucun lecteur sur les tablettes ou les téléphones cellulaires (iOS ou Android) n'offre ces fonctionnalités. Nous en sommes bien désolés.}

Ce document se veut un complément à ce qui sera fait en classe. La théorie est la même, mais les exemples sont différents. Consultez ce document avant les cours pour vous préparer ou après pour renforcer votre compréhension.

\subsection*{Fonctionnalités}

Lorsqu'un mot est bleu, c'est qu'il est possible de cliquer sur celui-ci pour aller quelque part dans le document\dans{ordi}{ ou pour faire apparaître de l'information}. 

\dans{ordi}{Dans les exercices, lorsqu'on passe la souris au-dessus d'un \creerInfobulle{Une réponse!}{infoExempleReponse}[faux] \infobulle{R}{infoExempleReponse}, la réponse de l'exercice s'affiche. Les réponses aux exercices sont aussi rassemblées dans un même chapitre à la fin du document.}
\dans{mob}{Les réponses aux exercices sont rassemblées dans un même chapitre à la fin du document. Vous pouvez alterner entre les questions et les réponses en cliquant sur le mot \og section \fg{} suivit de son numéro}

Les solutions des exemples ne sont pas affichées par défaut. Il faut cliquer pour les faire apparaître. Avant de le faire, posez-vous les questions suivantes :

\begin{center}
\begin{liste}
\item Est-ce que je comprends tous les mots de la question et celle-ci dans son ensemble? 

Si la réponse est négative, il faut revoir la définition des mots qui vous posent problème (un index se trouve à la fin des notes de cours) et relire le texte qui précède l'exemple. 

\item Est-ce que je comprends le lien entre cette question et la matière à l'étude? 

Si la réponse est négative, il faut relire le texte qui précède l'exemple. 


\item Est-ce que je suis capable de répondre à la question? 

Il est normal de ne pouvoir le faire s'il s'agit de l'un des premiers exemples d'une nouvelle matière. Essayez tout de même de trouver la raison de votre difficulté. Est-ce une étape en particulier? Est-ce pour démarrer le problème? Affichez ensuite la solution et, en la lisant, répondez à vos interrogations. 

Si vous croyez être en mesure de répondre à la question, alors faites-le. Écrivez votre démarche complète et, dans un deuxième temps, comparez-la avec la solution. Portez attention aux détails de présentation et à la notation. Si votre démarche est différente, faites-la  vérifier par votre professeur. 

Nous vous rappelons qu'à l'examen vous devrez faire des démarches complètes, qui respectent les notations mathématiques. Habituez-vous à le faire même en exercice sinon, il sera très difficile de réussir à l'examen.
\end{liste}
\end{center}


Dans les sections nommées \emph{Ressources additionnelles}, vous trouverez des liens vers des documents ou des vidéos qui pourraient vous aider en cas de difficulté.

\subsection*{Calculs et simplifications}

Une petite remarque concernant les exercices du présent document : les réponses ont été calculées sans arrondir dans les calculs, si bien que vous pourriez obtenir des réponses légèrement différentes si vous arrondissez lors de calculs intermédiaires. 

Voici les règles concernant la présentation des réponses pour le cours de calcul différentiel. Ces règles sont appliquées dans le document et elles servent de critères de correction des évaluations. 

\begin{liste}
\item Regrouper les termes semblables. Exemple : on ne laisse pas \(10x+3x\). On écrit $13x$. 

\item Simplifier les termes semblables au numérateur et au dénominateur. Exemple : on ne laisse pas $\frac{10x^2}{5x}$. On écrit $2x$. 

\item Effectuer les calculs simples. Exemples : $\sqrt{25} = 5$, $2^3 = 8$, $e^0=1$ et $\sin(\pi) = 0$. 

\item Ne pas laisser d'exposant négatif. Exemple : on ne laisse pas $x^{{-3}}$. On écrit $\frac{1}{x^3}$.

\item Ne pas laisser de fraction à plus de deux étages. Exemple : on ne laisse pas $\frac{\frac{x}{2}}{3}$. On écrit $\frac{x}{6}$. 

\item Factoriser lorsque cela engendre une simplification. Exemples : on peut laisser $20x-10$, mais l'expression $\frac{20x-10}{5}$ doit être réduite à $4x-2$.
\end{liste}

Les exercices de ce document ont été sélectionnés pour que leur solution se fasse sans calculatrice. Il y a cependant quelques exceptions (souvent lorsque les problèmes sont tirés d'applications réelles). Ces questions sont marquées du symbole \imaCalcu{}. %%%Notez que les calculs à faire pour résoudre ces exercices ne seraient pas demandés lors d'une évaluation.
\end{bloc}%mi

