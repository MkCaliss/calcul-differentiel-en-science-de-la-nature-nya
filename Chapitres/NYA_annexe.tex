% Copyright (C) 2019-2021 Simon Paquette
%
% Information de contact : latex.multidoc@gmail.com
%
% Il est permis de copier, distribuer ou modifier ce logiciel sous les 
% termes de la licence LaTeX project Public Licence (LPPL), version 1.3c 
% ou autres versions plus récentes.
%
% Le logiciel est fourni "tel quel", sans aucune garantie, qu'elle soit 
% explicite ou implicite, incluant, mais sans s'y limiter, les garanties 
% implicites de qualité marchande et de convenance à une fin particulière.
%
%
\chapter{Définitions et logique mathématique} \label{annexeA}
%
%
\begin{bloc}{im}
\begin{defi}[Théorème]{defThm}
Un \defTerme{théorème} est un énoncé qui est toujours vrai et qui peut être démontré.\index{Théorème} 

Dans un théorème, on trouve des hypothèses et une conclusion. Un théorème affirme que si les hypothèses sont vraies alors la conclusion l'est aussi.
\end{defi}
\end{bloc}%im
%
%
\begin{bloc}{im}
\begin{defi}[Lemme]{defLem}
Un \defTerme{lemme} est un énoncé qui est essentiellement comme un théorème, mais dont le résultat est moins important.\index{Lemme} 

Souvent, un lemme est énoncé et démontré dans le but de l'utiliser dans la démonstration d'un théorème. En faisant de la sorte, on simplifie la preuve du théorème en n'ayant pas à démontrer à même le théorème un résultat intermédiaire.
\end{defi}
\end{bloc}%im
%
%
\begin{bloc}{im}
\begin{defi}[Corollaire]{defCor}
Un \defTerme{corollaire} est un énoncé de la même forme qu'un théorème ou d'un lemme et qui découle directement d'un théorème.\index{Corollaire} 
\end{defi}
\end{bloc}%im
%
%
\begin{bloc}{m}
\begin{exemple}
Le théorème de Pythagore découle directement de la loi des cosinus. On aurait pu le nommer corollaire. Cependant, le théorème de Pythagore a probablement été prouvé avant la loi des cosinus et alors nommé théorème.
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{im}
\begin{defi}[Démonstration]{defDemo}
Une \defTerme{démonstration} est une suite d'étapes que l'on peut justifier.\index{Démonstration}

Elle sert à établir la véracité d'un énoncé.
\end{defi}
\end{bloc}%im
%
%
\begin{bloc}{m}
Souvent, les théorèmes ont pour formulation : \og Si ... alors ... \fg{}. Après le \og si \fg{}, on énumère les hypothèses et après le \og alors \fg{}, la conclusion. Le symbole mathématique pour représenter le \emph{si alors} est $\rightarrow$. 
\end{bloc}%m
%
%
\begin{bloc}{im}
\begin{defi}[Réciproque]{defReciproque}
La \defTerme{réciproque} d'un énoncé initial de la forme \og si $A$ alors $B$ \fg{} est une énoncé de la forme \og si $B$ alors $A$ \fg{} où $A$ représente les hypothèses et $B$ la conclusion de l'énoncé initial. Il s'agit d'un énoncé qui prend comme hypothèse la conclusion de l'énoncé initial et qui a comme conclusion les hypothèses de l'énoncé initial.\index{Réciproque}
\end{defi}
\end{bloc}%im
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Déterminer la réciproque de \og si $x>1$ alors $x^2 >1$\fg{}.}{

La réciproque est \og si $x^2>1$ alors $x >1$\fg{}.
}
\end{exemple}
%
%
\begin{attention}{attReciproque}
La réciproque d'un théorème n'est pas toujours vraie.
\end{attention}

Dans l'exemple précédent, la réciproque est vraie, mais pas dans l'exemple qui suit.

\begin{exemple}
\QS{Déterminer la réciproque de \og si $x>0$ alors $x^2>0$\fg{}.}{

La réciproque est \og si $x^2>0$ alors $x>0$ \fg{}. Ce qui est faux, car le carré d'un nombre négatif est positif.
}
\end{exemple}

Parfois, un théorème peut avoir la forme \og ... si et seulement si ... \fg{}. Le symbole pour le \emph{si et seulement si} est $\Leftrightarrow$. Le \emph{si et seulement si} est utilisé lorsque la réciproque de l'énoncé est vraie. 
\end{bloc}%m
%
%
\begin{bloc}{im}
\begin{defi}[Contraposée]{defContraposee}
La \defTerme{contraposée} d'un énoncé de la forme $A \Rightarrow B$ est $\lnot B \Rightarrow \lnot A$, où $\lnot A$ veut dire le contraire de $A$ (se lit non $A$).\index{Contraposée} 

Un énoncé et sa contraposée ont toujours la même valeur de véracité. Si l'un est vrai, alors l'autre aussi.
\end{defi}
\end{bloc}%im
%
%
\begin{bloc}{m}
\begin{exemple}
\QS{Déterminer la contraposée de \og si $x>1$ alors $x^2 > 1$ \fg{}.}{

La contraposée est \og si $x^2 \leq 1$ alors $x \leq 1$ \fg{}. On remarque que la contraposée aussi est vraie. 
}
\end{exemple}
\end{bloc}%m
%
%
\begin{bloc}{im}
\begin{defi}[Fontions inverses]{defFonctionsInverses}
Pour parler d'inverse, nous devons préciser l'opération utilisée. 

L'\defTerme{inverse additif} d'une fonction $f$ est une fonction $g$ telle que $f(x)+g(x)= 0$. 

L'\defTerme{inverse multiplicatif} d'une fonction $f$ est une fonction $g$ telle que $f(x)\cdot g(x)= 1$.\index{Fonction!inverse}
\end{defi}
\end{bloc}%im
%
%
\begin{bloc}{im}
\begin{defi}[Fontions réciproques]{defFonctionsReciproques}
Les fonctions $f$ et $g$ sont dites \defTerme{réciproques} si $f\big(g(x)\big) = x$ sur le domaine de $g$ et si $g\big(f(x)\big) = x$ sur le domaine de $f$. 

La \defTerme{réciproque} de $f$ est notée $f^{{-1}}$. \index{Fonction!réciproque}
\end{defi}
\end{bloc}%im
%
%
\begin{bloc}{m}
\rem{} Ne pas confondre $f^{{-1}}$ (la réciproque de $f$) avec $\big(f(x)\big)^{{-1}} = \dfrac{1}{f(x)}$ (l'inverse multiplicatif de $f$).

\begin{exemple}
Pour $x\geq 0$, $f(x) = x^2$ et $g(x)=\sqrt{x}$ sont des fonctions réciproques.
\end{exemple} 
\end{bloc}%m

