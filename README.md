Vous trouverez ci-dessous tout le matériel nécessaire pour l'enseignement du cours collégial de calcul différentiel en sciences de la nature (201-NYA-05).

Pour ce cours, la matière est présentée par type de fonctions. Ainsi, l'ensemble des concepts est vu trois fois : d'abord sur les fonctions polynominales, ensuite sur l'ensemble des fonctions algébriques et finalement sur l'ensemble des fonctions transcendantes. L'objectif est d'augmenter la compréhension et la rétention chez les étudiants et étudiantes. 

L'élaboration de ce matériel, par Julie Gendron, Simon Paquette et Jean-Nicolas Pépin, a débuté en 2017 et depuis il a été utilisé par plus d'une douzaine de professeurs et professeures. Le matériel est en constante évolution.

Le matériel est constitué de six fichiers :
- Un exemple d'échéancier.
- Un manuel numérique, au format PDF, optimisé pour la lecture à l'ordinateur avec l'application gratuite Acrobat Reader.
- Un manuel numérique, au format PDF, optimisé pour la lecture sur un appareil mobile.
- Des diapositives, au format PDF, pour les présentations en classe. Il est préférable d'utiliser l'application gratuite Acrobat Reader pour profiter pleinement des fonctionnalités.
- Des notes de cours à compléter par les étudiants et étudiantes. Le contenu des notes de cours est le même que celui des diapositives, mais adapté à un format papier lettre.
- Un document, au format PDF, qui contient exclusivement les séries d'exercices du manuel ainsi que leurs réponses. Ce document est offert aux élèves qui préfèrent ne pas avoir d'appareil électronique lorsqu'ils font des exercices.

Ces documents ont été créés à l'aide de la classe LaTeX [multidoc](https://gitlab.com/sipaq11/classe-latex-multidoc).
